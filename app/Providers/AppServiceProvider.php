<?php

namespace App\Providers;

use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*',function($view) {
            $categories = Category::where("parent_id",null)->get();
            $view->with('categories',$categories);
            $allCategories = Category::where("parent_id","!=",null)->get();
            $view->with('allCategories',$allCategories);
            $user = Auth::user();
            $fontSize = $user ? $user->font_size : 'sedang';
            $view->with('fontSize', $fontSize);
        });
    }
}
