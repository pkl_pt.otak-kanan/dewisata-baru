<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class AdminController extends Controller
{
    public function adminlistview(){
        return view("admin.dataadmin");
    }
    public function getAdminList(Request $request)
    {
        if ($request->ajax()) {
            $pengguna = User::where('role', 'admin')->get();
            return DataTables::of($pengguna)
                ->addColumn('action', function ($user) {
                    return '
                        <a href="' . route('admin.edit_admin', $user->id) . '" class="btn btn-success btn-sm">Edit</a>
                        <form method="post" action="' . route('admin.delete_admin', $user->id) . '" style="display: inline;">
                            ' . csrf_field() . '
                            ' . method_field('DELETE') . '
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(\'Are you sure you want to delete this admin?\')">Delete</button>
                        </form>
                    ';
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.dataadmin');
    }
    public function createAdmin()
    {
        return view('admin.createadmin');
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            // Validasi data yang dikirim dari form tambah admin
            $request->validate([
                'username' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:8', // Sesuaikan aturan yang diinginkan
            ]);

            // Simpan admin baru ke dalam database
            User::create([
                'name' => $request->input('username'),
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
                'role' => 'admin', // Tambahkan peran admin sesuai aturan Anda
                'theme' => 'light', // Default value untuk kolom 'theme'
                'font_size' => 'sedang', // Default value untuk kolom 'font_size'
                'whatsapp' => null, // Isi dengan nilai NULL
                'alamat' => null,
            ]);

            // Redirect ke halaman data admin dengan pesan sukses
            return redirect()->route('admin.data_all')->with('success', 'Admin baru berhasil ditambahkan.');
        }
     
    }

    public function editAdmin($id)
    {
        $admin = User::findOrFail($id);
        return view('admin.editadmin', compact('admin'));
    }

    public function updateAdmin(Request $request, $id)
    {
        $admin = User::findOrFail($id);

        $request->validate([
            'username' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . $id,
            'password' => 'nullable|string|min:8', // Sesuaikan aturan yang diinginkan
        ]);

        $data = [
            'name' => $request->input('username'),
            'email' => $request->input('email'),
        ];

        if ($request->has('password')) {
            $data['password'] = bcrypt($request->input('password'));
        }

        $admin->update($data);

        return redirect()->route('admin.data_all')->with('success', 'Admin berhasil diperbarui.');
    }

    public function deleteAdmin($id)
    {
        $admin = User::findOrFail($id);
        $admin->delete();
        return redirect()->route('admin.data_all')->with('success', 'Admin berhasil dihapus.');
    }


     public function laporanPenjualan(Request $request)
    {
        $yearFilter = $request->input('year', date('Y'));
        $years = range(2020, 2030);

        $monthlyRevenue = DB::table('invoices')
            ->select(DB::raw('MONTH(invoices.created_at) as month'), DB::raw('SUM(orders.price * orders.quantity) as revenue'))
            ->join('orders', 'invoices.id', '=', 'orders.invoice_id')
            ->where('invoices.status', 'paid')
            ->whereYear('invoices.created_at', $yearFilter)
            ->groupBy(DB::raw('MONTH(invoices.created_at)'))
            ->pluck('revenue', 'month');

        $totalRevenue = 0;
        foreach (range(1, 12) as $month) {
            $monthlyRevenue[$month] = $monthlyRevenue[$month] ?? 0;
            $totalRevenue += $monthlyRevenue[$month];
        }

        return view('admin.laporan-penjualan', compact('monthlyRevenue', 'totalRevenue', 'yearFilter', 'years'));
    }
}
