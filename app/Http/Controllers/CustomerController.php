<?php

namespace App\Http\Controllers;
use App\Models\User;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CustomerController extends Controller
{
    public function users (Request $request)
    {
        $pengguna = User::where('role', 'user')->get();
        return view('admin.users', compact('pengguna'));
    }

    public function getUsersCount()
    {
        $usersCount = User::where('role', 'user')->count();
        return $usersCount;
    
    }
}
