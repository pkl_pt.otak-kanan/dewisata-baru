<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Price;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Invoice;

class CartController extends Controller {

    public function getAllCart(){
        $cartItems = \Cart::getTotal();
        return $cartItems;
    }

   public function cartList()
    {
        $isContinue=true;
        $cartItems=[];
        $carts = \Cart::getContent();
        // return $carts;
        foreach ($carts as $item) {
            $product = Product::find($item->id);
            // $item['kuota']=$product->kuota;
            // return $item;
            $orders = Order::where('product_id',$item->id)->where("booking_date",Carbon::today())->get();
            // return $orders;
            if($orders->count()>0){
                foreach($orders as $order){
                    if($order){
                        if($order->invoice){
                            if($order->invoice->status=="paid"){
                                $kuota = $order->product->kuota;
                                $kuota-=$orders->sum('quantity');
                                $kuota-=$item->quantity;
                                $item['kuota']=$kuota;
                                // $item['quantity']=$order->quantity;
                                $item['category_name']=$order->product->category->name;
                                if(($order->product->category->name=='FREE OPEN TRIP')&&($item['kuota']<0)){
                                    $isContinue=false;
                                }
                                // $cartItems[]=$item;
                                // $item->product->prices;
                                // array_push($cartItems,$item);
                            }
                        }
                    }
                }
                $item['kuota']=$product->kuota;
                $item['category_name']=$product->category->name;
            }else{
                $kuota = $product->kuota;
                $kuota-=$item->quantity;
                // $item['id']=$item->id;
                $item['category_name']=$product->category->name;
                if($product->category->name=='FREE OPEN TRIP'){
                    if($kuota<0){
                        $isContinue=false;
                    }
                    $item['kuota']=$kuota;
                }else{
                    $item['kuota']=$kuota;
                }
                
                // array_push($cartItems,$item);
                // $cartItems[]=$item;
            }
           // $kuota = $product ? $product->kuota : 'N/A';
            // $categoryName = $product->category->name ?? '';

            // $item->kuota = $kuota;
            $cartItems[]=$item;
            // array_push($cartItems,$item);
            // $item->category_name = $categoryName;
        }
        // return response()->json(['data'=>$cartItems,'isContinue'=>$isContinue]);
        return view('cart', compact('cartItems','isContinue'));
    }

    public function addToCart(Request $request)
    {
        // return $request->all();
        $price = Price::where('id', $request->plan_id)->first();
        $cartItem = \Cart::add([
            'id' => $price->product->id,
            'name' => $price->product->name,
            'price' => $price->price,
            'quantity' => $request->qty,
        ]);

        session()->flash('success', 'Product is Added to Cart Successfully!');

        return redirect()->route('cart.list');
    }

    public function updateCart(Request $request){
        if($request->quantity>0){
        \Cart::update(
            $request->id,
            [
                'quantity' => [
                    'relative' => false,
                    'value' => $request->quantity
                ],
            ]
        );

        session()->flash('success', 'Item Cart is Updated Successfully !');
    }else{
        \Cart::remove($request->id);
        session()->flash('success', 'Item Cart Remove Successfully !');
    }

        return redirect()->route('cart.list');
    }

    public function removeCart(Request $request){
        \Cart::remove($request->id);
        session()->flash('success', 'Item Cart Remove Successfully !');

        return redirect()->route('cart.list');
    }

    public function clearAllCart(){
        \Cart::clear();

        session()->flash('success', 'All Item Cart Clear Successfully !');

        return redirect()->route('cart.list');
    }

    public function order(Request $request){
        return $request->all();
    }

    public function createOrder(Request $request)
    {

    if (!Auth::check()) {return redirect()->route('login')->with([
        'errorlogin' => 'Maaf, Anda harus login terlebih dahulu !',
        'alert-duration' => 2000,
    ]);}
    $isContinue=true;
    $cartItems=[];
    $carts = \Cart::getContent();
    $date = Carbon::createFromFormat('Y-m-d', $request->booking_date);
    foreach ($carts as $item) {
        $product = Product::find($item->id);
        // return $product;
        $orders = Order::where('product_id',$item->id)->where("booking_date",$date->format("Y-m-d"))->get();
        if($orders->count()>0){
            foreach($orders as $order){
                if($order){
                    if($order->invoice){
                        if($order->invoice->status=="paid"){
                            $kuota = $order->product->kuota;
                            $kuota-=$orders->sum('quantity');
                            $kuota-=$item->quantity;
                            $item['kuota']=$kuota;
                            // $item['quantity']=$order->quantity;
                            $item['category_name']=$order->product->category->name;
                            if(($order->product->category->name=='FREE OPEN TRIP')&&($item['kuota']<0)){
                                $isContinue=false;
                            }
                        }
                        $item['category_name']=$product->category->name;
                    }
                }
            }
        }else{
            $kuota = $product->kuota;
            $kuota-=$item->quantity;
            // $item['id']=$item->id;
            $item['category_name']=$product->category->name;
            if($product->category->name=='FREE OPEN TRIP'){
                if($kuota<0){
                    $isContinue=false;
                }
                $item['kuota']=$kuota;
            }else{
                $item['kuota']=$kuota;
            }
            // array_push($cartItems,$item);
        }
        array_push($cartItems,$item);
    }
    if($isContinue){

    $cartItems = \Cart::getContent();

    // Gunakan transaksi untuk memastikan seluruh pemesanan berjalan sebagai satu kesatuan
    DB::beginTransaction();

    try {
        // Simpan data invoice
        $inv = new Invoice();
        $inv->user_id = Auth::user()->id;
        $inv->status = "pending";
        $inv->code=$this->generateCode();
        $inv->save();

        foreach ($cartItems as $item) {
            $product = Product::find($item->id);



            // Simpan data order dan hubungkan ke invoice
            $order = new Order();
            $order->code = $this->generateCode();
            $order->description = "";
            $order->status = 'pending';
            $order->invoice_id = $inv->id; // Hubungkan order ke invoice yang baru dibuat
            $order->product_id = $item->id;
            $order->user_id = auth()->user()->id;
            $order->price = $item->price;
            $order->quantity = $item->quantity;
            $order->booking_date = $request->booking_date;
            $order->save();
        }

            // Jika semua produk tersedia dan pemesanan berhasil, commit transaksi
            DB::commit();

            \Cart::clear();
            return response()->json(['success' => true, 'message' => 'Pemesanan berhasil.']);
        } catch (\Exception $e) {
            // Jika terjadi kesalahan, rollback transaksi dan tampilkan pesan error
            DB::rollBack();
            return response()->json(['success' => false, 'message' => 'Terjadi kesalahan saat memproses pemesanan.' . $e]);
        }

    }else{
        return response()->json(['data'=>$cartItems,'isContinue'=>$isContinue,'success'=>$isContinue,'message'=>'Kuota habis!']);
    }

    }
    public function generateCode() {
        $number = mt_rand(1000000, 9999999); // better than rand()

        // call the same function if the barcode exists already
        if ($this->codeNumberExists($number)) {
            return $this->generateCode();
        }

        // otherwise, it's valid and can be used
        return $number;
    }

    public function codeNumberExists($number) {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return Invoice::where('code',$number)->exists();
    }
}
