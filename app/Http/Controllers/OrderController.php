<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Order;
use App\Models\Invoice;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller {

    public function completed($order_id) {
        $order = Order::find($order_id);
        return view('completed', compact('order'));
    }

    public function pemesanan(Request $request)
    {
        if ($request->ajax()) {
            $allOrders = Invoice::with(['user', 'orders.product'])
                ->orderBy('created_at', 'desc')
                ->get();

            return DataTables::of($allOrders)
                ->addColumn('customer', function ($order) {
                    return '<a href="' . route('admin.customer', ['userId' => $order->user->id]) . '" class="customer-link">' . $order->user->name . '</a>';
                })
                ->addColumn('total', function ($order) {
                    return  "Rp" . number_format($order->orders->sum(function ($item) {
                        return $item->price * $item->quantity;
                    }), 0);
                })
                ->addColumn('product', function ($order) {
                    $products = '';
                    $products .= '<ul>';
                    foreach ($order->orders as $o) {
                        $date = Carbon::createFromFormat('Y-m-d', $o->booking_date);
                        $products .= '<li>';
                        $products .= $o->product->name . ' <br>(' . $o->quantity . 'x) Rp' . number_format($o->price, 0) . ' • (' . $date->format('d-m-Y') . ')';
                        $products .= '</li>';
                    }
                    $products .= '</ul>';
                    return $products;
                })
                ->editColumn('created_at', function ($invoice) {
                    return $invoice->created_at->format('d-m-Y • H:i');
                })
                ->addColumn('action', function ($order) {
                    $buttons = '';
                    if ($order->status == 'cancel') {
                        $buttons .= '<button type="submit" class="btn btn-sm btn-danger" disabled>dibatalkan</button>';
                    } elseif ($order->status == 'paid') {
                        $buttons .= '<button type="submit" class="btn btn-sm btn-success" disabled>terbayar</button>';
                    } elseif ($order->status == 'pending') {
                        $buttons .= '<div class="btn-group" role="group">';

                        $buttons .= '<form action="' . route('admin.orders.pay', $order->id) . '" method="POST" style="margin-right: 5px;">';
                        $buttons .= csrf_field();
                        $buttons .= '<button type="submit" class="btn btn-sm btn-success">Dibayar</button>';
                        $buttons .= '</form>';

                        $buttons .= '<form action="' . route('admin.orders.cancel', $order->id) . '" method="POST" style="margin-right: 5px;">';
                        $buttons .= csrf_field();
                        $buttons .= '<button type="submit" class="btn btn-sm btn-danger">Batalkan</button>';
                        $buttons .= '</form>';

                        $buttons .= '</div>';
                    }
                    return $buttons;
                })
                ->rawColumns(['customer', 'product', 'action'])
                ->make(true);
        }

        $acceptedOrder = Session::get('acceptedOrder');
        $allOrders = Invoice::whereIn('status', ['belum dibayar', 'Terbayar', 'Dibatalkan'])
            ->orderBy('created_at', 'desc')
            ->whereNotIn('id', [$acceptedOrder ? $acceptedOrder->id : 0])
            ->get();

        return view('admin.orders.pemesanan', compact('allOrders', 'acceptedOrder'));
    }

    public function pay(Order $order)
    {
        $order->status = 'paid';
        $order->save();

        // Update status invoice (jika perlu)
        $invoice = $order->invoice;
        $invoice->status = 'paid';
        $invoice->save();

         Session::put('acceptedOrder', $order);

        return redirect()->back()->with('success', 'Pesanan sudah dibayar');
    }

    public function cancel(Order $order)
    {
        $order->status = 'cancel';
        $order->save();

        // Update status invoice (jika perlu)
        $invoice = $order->invoice;
        $invoice->status = 'cancel';
        $invoice->save();

        if (Session::has('acceptedOrder') && Session::get('acceptedOrder')->id == $order->id) {
            Session::forget('acceptedOrder');
        }

        return redirect()->back()->with('danger', 'Pesanan dibatalkan');
    }

   public function accept(Order $order) {
    // Ubah status pada invoice
    $order->status = 'Diproses';
        $order->save();
        
        Session::put('acceptedOrder', $order);

        return redirect()->back()->with('success', 'Pesanan diterima');
    }


public function reject(Order $order) {
    // Ubah status pada invoice
    $order->status = 'Ditolak';
        $order->save();

    // Ubah status pada order yang terkait dengan invoice yang sama

    return redirect()->back()->with('danger', 'Pesanan ditolak');
}

    public function getOrderCount() {
        $orderCount = Invoice::where('status', 'pending')->count();
        return $orderCount;
    }

    public function customer(Request $request)
    {
        $userId = $request->userId;
        $order = Order::where('user_id', $userId)->first();

        if (!$order) {
            return redirect()->back()->with('error', 'Pesanan tidak ditemukan.');
        }
        $user = $order->user;
        return view('admin.customer', compact('user'));
    }
}
