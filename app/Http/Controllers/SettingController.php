<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function settings(Request $request)
    {
        $user = $request->user();
        $darkMode = $user->theme === 'dark';
        $fontSize = $user->font_size;

        return view('admin.settings', [
            'darkMode' => $darkMode,
            'fontSize' => $fontSize,
        ]);
    }

    public function saveSettings(Request $request)
    {
        $darkMode = $request->input('mode') === 'dark';
        $fontSize = $request->input('fontSize', 'sedang');
        
        $user = $request->user();
        $user->theme = $darkMode ? 'dark' : 'light'; // Mengubah nilai kolom theme sesuai dengan mode
        $user->font_size = $fontSize;
        $user->save();

        return redirect()->route('admin.settings')->with('success', 'Pengaturan berhasil disimpan.');
    }
}
