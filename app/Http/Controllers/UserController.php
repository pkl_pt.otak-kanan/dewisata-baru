<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\User;
use App\Models\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function histori()
    {
        $user = Auth::user();
        if(!$user){
            return redirect()->to('login');
        }
        $invoices = Invoice::where('user_id', $user->id)->orderBy('created_at','DESC')->get();
        return view('historipemesanan', compact('invoices'));
    }

    public function myProfile()
    {
        $user = Auth::user();
        return view('myprofile', compact('user'));
    }

    public function updateProfile(Request $request)
{
    $user = User::find(Auth::id());
    $user->name = $request->input('name');
    $user->email = $request->input('email');
    $user->whatsapp = $request->input('whatsapp');
    $user->save();

    return redirect()->route('my-profile')->with('success', 'Profile updated successfully!');
}

}
