<?php

namespace App\Http\Controllers;

use App\Models\Type;
use App\Models\Group;
use App\Models\Price;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cat = Category::where("slug",$request->cat)->first();
        if ($request->ajax()) {
            $cat = Category::where("slug",$request->cat)->first();
            $data = Product::where("category_id",$cat->id)->get();
            
            foreach ($data as $product) {
            $product->category_name = $product->category->name;
        }
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row) use($request){
                    $btn = '<a href="'.route('admin.products.edit', ['id' => $row->id,'cat'=>$request->cat]).'" class="edit btn btn-info btn-sm">Edit</a> |
                    <a href="/detail/'.$row->slug.'" class="edit btn btn-success btn-sm">View</a> |
                    <a href="/delete.products/'.$row->id.'" class="delete btn btn-danger btn-sm">Delete</a>';
                        return $btn;
                    })
                    ->addColumn('image', function($row) use($request){
                        $image = '<img width="100" height="100" src="/images/'.$row->image.'">';
                            return $image;
                        })
                    ->rawColumns(['action','image'])
                    ->make(true);
        }
        return view('admin.products.index',compact('cat'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $groups = Group::all();
        $cat = Category::where('slug',$request->cat)->first();
        $services = Category::where("parent_id","=",$cat->id)->get();
        $types = Type::where("category_id",$cat->id)->get();
        return view('admin.products.create',compact("cat","types","services","groups"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $p = new Product();
        $p->name = $request->name;
        $p->slug = Str::slug($request->name);
        $p->category_id = $request->category_id;
        $p->type_id=$request->type_id;
        $p->group_id=$request->group_id;
        $p->code = $request->code;
        $p->description=$request->description;
        $p->kuota = $request->kuota === null ? 0 : $request->kuota;

        if ($request->hasFile('image')) {
        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $image->move(public_path('/images'), $imageName);
        $p->image = $imageName;
    }
        $p->save();
        $prices = $request->price;
        foreach($request->label as $key=>$label){
            $price = new Price();
            $price->name=$label;
            $price->price=$prices[$key];
            $price->product_id=$p->id;
            $price->save();
        }

        return redirect()->back()->with([
            'success' => 'Berhasil Menambahkan Data',
            'alert-duration' => 2000,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::where("slug",$id)->first();
        $products = Product::where("category_id",$product->category_id)->get();
        return view("front.detail-product",compact('product','products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id,Request $request)
    {
       $cat = Category::where("slug", $request->cat)->firstOrFail();
        $product = Product::findOrFail($id);    
        $groups = Group::all();
        // $cat = Category::where('id', $product->category_id)->first();
        $services = Category::where("parent_id", "=", $cat->id)->get();
        // return $cat;
        $types = Type::where("category_id", $cat->id)->get();
        $categories = Category::all();
        $description = $product->description;
        return view('admin.products.edit', compact('product','description', 'cat', 'types', 'services', 'groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request, $id)
{
    $product = Product::findOrFail($id);
    $product->name = $request->name;
    $product->slug = Str::slug($request->name);
    $product->category_id = $request->category_id;
    $product->type_id = $request->type_id;
    $product->group_id = $request->group_id;
    $product->code = $request->code;
    $product->description = $request->description;
    $product->kuota = $request->kuota === null ? 0 : $request->kuota;
    $file_path = app_path().'/images/'.$product->image;
    if(file_exists($file_path)){
        unlink($file_path);
    }
    if ($request->hasFile('image')) {
        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $image->move(public_path('/images'), $imageName);
        $product->image = $imageName;
    }
    $product->save();

    $prices = $request->price;
    Price::where('product_id', $id)->delete();

    foreach ($request->label as $key => $label) {
        $price = new Price();
        $price->name = $label;
        $price->price = $prices[$key];
        $price->product_id = $product->id;
        $price->save();
    }

     return redirect()->to('/admin/products?cat='.$product->category->slug)->with('success', 'Produk berhasil diperbarui');
}

    public function destroy($id)
    {
        product::where('id', $id)->delete();
        return redirect()->back()->with([
            'success' => 'Berhasil Menghapus Data',
            'alert-duration' => 2000,
        ]);
    }

    public function showByCategory($slug){
        $category = Category::where("slug",$slug)->first();
        $products = Product::where("category_id",$category->id)->get();
        return view('front.category-product',compact('category','products'));
    }

    public function getProductsCount()
    {
        $productsCount = Product::all()->count();
        return $productsCount;
    }
}
