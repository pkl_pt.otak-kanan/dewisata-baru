<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
   function index (){
       return view('auth.login');
   }

   function login(Request $request){
    $request->validate([
        'email' => 'required',
        'password' => 'required',
    ],[
        'email.required' => 'Email wajib diisi!',  
        'password.required' => 'Password wajib diisi!',  
    ]);

    $loginadmin = [
        'email' => $request->email,
        'password' => $request->password,
    ];

    if (Auth::attempt($loginadmin, $request->has('remember'))) {
            if ($request->has('remember')) {
                setcookie('email', $request->email, time() + (86400 * 30), "/"); 
                setcookie('password', $request->password, time() + (86400 * 30), "/"); 
            } else {
                setcookie('email', '', time() - 3600, "/"); 
                setcookie('password', '', time() - 3600, "/");
            }

            return redirect('admin/dashboard');
        } else {
            return redirect()->back()->with("error_message", "Username atau Password anda salah");
        }
						
}

 public function logout_admin()
{
    Auth::logout();
    return redirect()->route('login')->with([
        'logout_success' => 'Berhasil Logout',
        'alert-duration' => 2000,
    ]);
}
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;

    // /**
    //  * Where to redirect users after login.
    //  *
    //  * @var string
    //  */
    // protected $redirectTo = RouteServiceProvider::HOME;

    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('guest')->except('logout');
    // }
}
