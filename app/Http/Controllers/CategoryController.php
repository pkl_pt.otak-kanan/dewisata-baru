<?php

namespace App\Http\Controllers;

use App\Models\Type;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cat = Category::where("slug","=",$request->cat)->first();
        if ($request->ajax()) {
            $data = Category::where("parent_id",$cat->id)->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row) use($cat){
                        $btn = '<a href="'.route('admin.categories.edit',['id' => $row->id,'cat'=>$cat->slug]).'" class="edit btn btn-info btn-sm">Edit</a> |
                        <a href="/delete.categories/'.$row->id.'" class="delete btn btn-danger btn-sm">Delete</a>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view("admin.categories.index",compact("cat"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // $types = Type::all();
        // $cat = Category::where('slug', $request->cat)->first();
        // return view("admin.categories.create", compact('types', 'cat'));
        $cat = Category::where("slug","=",$request->cat)->first();
        $types = Type::where('category_id', null)->orWhere('category_id', $request->cat)->get();
        return view("admin.categories.create", compact('types','cat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->parent_id>0){
            $cat = new Category();
            $cat->parent_id=$request->parent_id;
            $cat->name = $request->name;
            $cat->slug = Str::slug($request->name);
            $cat->save();
            return redirect()->back();
        }else{
            $cat = new Category();
            // $cat->parent_id=$request->parent_id;
            $cat->name = $request->name;
            $cat->slug = Str::slug($request->name);
            $cat->save();
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $cat = Category::where("slug","=",$request->cat)->first();
        $category = Category::where("id","=",$id)->first();
        // $types = Type::where('category_id', null)->orWhere('category_id', $request->cat)->get();
        return view("admin.categories.edit", compact('category','cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cat = Category::where('id',$id)->first();
        // return $type;
        $cat->name = $request->name;
        $cat->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        category::where('id', $id)->delete();
        return redirect()->back()->with([
            'success' => 'Berhasil Menghapus Data',
            'alert-duration' => 2000,
        ]);
    }

    public function getCategoryCount(){
        $categoryCount = Category::all()->count();
        return $categoryCount;
    }

    public function allCategories()
    {
        // Query untuk mengambil seluruh kategori dari database
        $categories = Category::all();

        // Menggunakan DataTables facade untuk memformat data agar sesuai dengan tampilan DataTable
        return DataTables::of($categories)
            ->addIndexColumn()
            ->addColumn('action', function($category) {
                // Ganti "admin.categories.edit" dengan route atau URL untuk halaman edit kategori
                $editUrl = route('admin.categories.edit', ['id' => $category->id, 'cat' => $category->slug]);
                // Ganti "/delete.categories/" dengan route atau URL untuk menghapus kategori
                $deleteUrl = '/delete.categories/' . $category->id;

                $buttons = '<a href="' . $editUrl . '" class="edit btn btn-info btn-sm">Edit</a> |
                    <a href="' . $deleteUrl . '" class="delete btn btn-danger btn-sm">Delete</a>';
                return $buttons;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function categoryListView()
    {
        return view('admin.categorylist');
    }
}
