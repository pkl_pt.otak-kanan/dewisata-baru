<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ProductListController extends Controller
{
    public function allProducts(Request $request)
    {
    $produk = Product::query();
    $catname = $request->input('cat');
    if ($catname) {
        $produk->whereHas('category', function ($query) use ($catname) {
            $query->where('name', $catname);
        });
    }

    return DataTables::of($produk)
        ->addColumn('jenis_produk', function ($product) {
            return $product->category->name;
        })
        ->addColumn('action', function($product) {
            $editUrl = route('admin.products.edit', ['id' => $product->id, 'cat' => $product->category->slug]);
            $deleteUrl = '/delete.products/' . $product->id;

            $buttons = '<a href="' . $editUrl . '" class="edit btn btn-info btn-sm">Edit</a> |
                <a href="' . $deleteUrl . '" class="delete btn btn-danger btn-sm">Delete</a>';
            return $buttons;
        })
        ->addColumn('image', function($product) {
            return '<img src="' . asset('images/' . $product->image) . '" width="100" height="100" />';
        })
        ->rawColumns(['action', 'image'])
        ->make(true);
    }
    
    public function getProductsCount()
    {
        $productsCount = Product::all()->count();
        return $productsCount;
    }

    public function productlistView(){
        return view('admin.productlist');
    }
}
