<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileAdminController extends Controller
{
    public function profile (){
        $user = Auth::user();
        return view('admin.profile', compact('user'));
    }
}
