<?php

namespace App\Http\Controllers;

use App\Models\Type;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cat = Category::where("slug","=",$request->cat)->first();
        // return $cat;
        if ($request->ajax()) {
            $data = Type::where("category_id",$cat->id)->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row) use($cat){
                        $btn = '<a href="'.route('admin.types.edit',['id' => $row->id,'cat'=>$cat->slug]).'" class="edit btn btn-info btn-sm">Edit</a> |
                        <a href="/delete.types/'.$row->id.'" class="delete btn btn-danger btn-sm">Delete</a>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view("admin.types.index",compact("cat"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // $types = Type::all();
        // $cat = Category::where('slug', $request->cat)->first();
        // return view("admin.categories.create", compact('types', 'cat'));
        $cat = Category::where("slug","=",$request->cat)->first();
        $types = Type::where('category_id', null)->orWhere('category_id', $request->cat)->get();
        return view("admin.types.create", compact('types','cat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->parent_id>0){
            $cat = new Type();
            $cat->category_id=$request->parent_id;
            $cat->name = $request->name;
            $cat->slug = Str::slug($request->name);
            $cat->save();
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $cat = Category::where("slug","=",$request->cat)->first();
        $type = Type::where('id',$id)->first();
        // $types = Type::where('category_id', null)->orWhere('category_id', $request->cat)->get();
        return view("admin.types.edit", compact('type','cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $id;
        $type = Type::where('id',$id)->first();
        // return $type;
        $type->name = $request->name;
        $type->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
