<?php

namespace App\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
        'code',
        'description',
        'status',
        'product_id',
        'user_id',
        'price',
        'quantity',
        'booking_date',
        'booking',
    ];

    // Relasi dengan model Product
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    // Relasi dengan model User
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function getTotalAttribute()
    {
        return $this->quantity * $this->price;
    }
    public function getDateBookingAttribute()
    {
        $date = Carbon::createFromFormat('Y-m-d', $this->booking_date); 
        return $date;
    }
    public function getBookingAttribute()
    {
        $date = Carbon::createFromFormat('Y-m-d', $this->booking_date); 
        return '('.$date->format('d-m-Y').')';
    }
    public function invoice(){
        return $this->belongsTo(Invoice::class);
    }


}
