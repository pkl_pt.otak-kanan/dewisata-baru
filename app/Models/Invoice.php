<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

    use HasFactory;

    protected $fillable = ['user_id', 'status', 'code'];

    public function orders(){
        return $this->hasMany(Order::class);
    }
    public function getTotalAmountAttribute(){
        return $this->orders->sum('total');
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}
