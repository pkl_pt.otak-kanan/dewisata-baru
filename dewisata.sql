-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 27 Jun 2023 pada 14.17
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dewisata`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'DMC', 'dmc', NULL, NULL, NULL),
(2, 'MICE ORGANIZER', 'mice-organizer', NULL, NULL, NULL),
(3, 'DROOM SERVICE', 'droom-service', NULL, NULL, NULL),
(4, 'TRANSPORT SERVICE', 'transport-service', NULL, NULL, NULL),
(5, 'DETICK SERVICE', 'detick-service', NULL, NULL, NULL),
(6, 'FREE OPEN TRIP', 'free-open-trip', 1, '2023-06-20 23:13:26', '2023-06-20 23:13:26'),
(7, 'EASY PRIVATE TRIP', 'easy-private-trip', 1, '2023-06-20 23:22:50', '2023-06-20 23:22:50'),
(8, 'FUN ACTIVITY PROGRAM', 'fun-activity-program', 1, '2023-06-20 23:23:06', '2023-06-20 23:23:06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `groups`
--

CREATE TABLE `groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `groups`
--

INSERT INTO `groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'FIT', NULL, NULL),
(2, 'GROUP', NULL, NULL),
(3, 'FiT & GIT', NULL, NULL),
(4, 'GIT', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_06_20_091126_create_categories_table', 2),
(6, '2023_06_20_091222_create_groups_table', 3),
(7, '2023_06_20_091426_create_types_table', 3),
(8, '2023_06_20_091651_create_products_table', 3),
(9, '2023_06_20_092511_create_prices_table', 4),
(10, '2023_06_20_092607_create_orders_table', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `prices`
--

CREATE TABLE `prices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `prices`
--

INSERT INTO `prices` (`id`, `name`, `price`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 'Harga Basic', 100000, 1, '2023-06-21 00:17:02', '2023-06-21 00:17:02'),
(2, 'Harga Medium', 500000, 1, '2023-06-21 00:17:02', '2023-06-21 00:17:02'),
(3, 'BESIC', 10000, 4, '2023-06-21 00:20:25', '2023-06-21 00:20:25'),
(4, 'MEDIUM', 25000, 4, '2023-06-21 00:20:25', '2023-06-21 00:20:25'),
(5, 'MEDIUM', 10000, 5, '2023-06-21 00:22:00', '2023-06-21 00:22:00'),
(6, 'BASIC', 10000, 5, '2023-06-21 00:22:00', '2023-06-21 00:22:00'),
(7, 'BASIC', 1000, 6, '2023-06-21 00:30:37', '2023-06-21 00:30:37'),
(8, 'MEDIUM', 100000, 6, '2023-06-21 00:30:37', '2023-06-21 00:30:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `type_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `description`, `code`, `image`, `group_id`, `user_id`, `category_id`, `type_id`, `created_at`, `updated_at`) VALUES
(1, 'FREE OPEN TRIP DAILY', 'free-open-trip-daily', '<p><span style=\"caret-color: rgb(85, 85, 85); color: rgb(85, 85, 85);\">Mea appareat omittantur eloquentiam ad, nam ei quas&nbsp;</span><strong style=\"font-weight: 600; -webkit-font-smoothing: antialiased; caret-color: rgb(85, 85, 85); color: rgb(85, 85, 85);\">oportere democritum</strong><span style=\"caret-color: rgb(85, 85, 85); color: rgb(85, 85, 85);\">. Prima causae admodum id est, ei timeam inimicus sed. Sit an meis aliquam, cetero inermis vel ut. An sit illum euismod facilisis, tamquam vulputate pertinacia eum at.</span><br></p>', '111-FOTD', 'https://prod-mayang.sgp1.digitaloceanspaces.com/img/1184495/1667617072/eyJ3aWR0aCI6NjQwLCJoZWlnaHQiOjY0MCwia2Jfc2l6ZSI6NTMwLjkxNn0%3D.jpeg', 1, NULL, 1, 1, '2023-06-21 00:17:02', '2023-06-21 00:17:02'),
(4, 'FREE OPEN TRIP WEEKLY', 'free-open-trip-weekly', '<p><span style=\"caret-color: rgb(85, 85, 85); color: rgb(85, 85, 85);\">Mea appareat omittantur eloquentiam ad, nam ei quas&nbsp;</span><strong style=\"font-weight: 600; -webkit-font-smoothing: antialiased; caret-color: rgb(85, 85, 85); color: rgb(85, 85, 85);\">oportere democritum</strong><span style=\"caret-color: rgb(85, 85, 85); color: rgb(85, 85, 85);\">. Prima causae admodum id est, ei timeam inimicus sed. Sit an meis aliquam, cetero inermis vel ut. An sit illum euismod facilisis, tamquam vulputate pertinacia eum at.</span><br></p>', '112-FOTW', 'https://prod-mayang.sgp1.digitaloceanspaces.com/img/1184495/1663985401/eyJ3aWR0aCI6NjQwLCJoZWlnaHQiOjY0MCwia2Jfc2l6ZSI6NzcyLjU4OH0%253D.jpeg', 1, NULL, 1, 2, '2023-06-21 00:20:25', '2023-06-21 00:20:25'),
(5, 'FREE OPEN TRIP MONTHLY', 'free-open-trip-monthly', '<p><span style=\"caret-color: rgb(85, 85, 85); color: rgb(85, 85, 85);\">Mea appareat omittantur eloquentiam ad, nam ei quas&nbsp;</span><strong style=\"font-weight: 600; -webkit-font-smoothing: antialiased; caret-color: rgb(85, 85, 85); color: rgb(85, 85, 85);\">oportere democritum</strong><span style=\"caret-color: rgb(85, 85, 85); color: rgb(85, 85, 85);\">. Prima causae admodum id est, ei timeam inimicus sed. Sit an meis aliquam, cetero inermis vel ut. An sit illum euismod facilisis, tamquam vulputate pertinacia eum at.</span><br></p>', '113-FOTM', 'https://prod-mayang.sgp1.digitaloceanspaces.com/img/1184495/1667466240/eyJ3aWR0aCI6NjQwLCJoZWlnaHQiOjY0MCwia2Jfc2l6ZSI6NTE3LjQyOX0%3D.jpeg', 1, NULL, 1, 3, '2023-06-21 00:22:00', '2023-06-21 00:22:00'),
(6, 'FREE OPEN TRIP YEARLY', 'free-open-trip-yearly', '<p><span style=\"caret-color: rgb(85, 85, 85); color: rgb(85, 85, 85);\">Mea appareat omittantur eloquentiam ad, nam ei quas&nbsp;</span><strong style=\"font-weight: 600; -webkit-font-smoothing: antialiased; caret-color: rgb(85, 85, 85); color: rgb(85, 85, 85);\">oportere democritum</strong><span style=\"caret-color: rgb(85, 85, 85); color: rgb(85, 85, 85);\">. Prima causae admodum id est, ei timeam inimicus sed. Sit an meis aliquam, cetero inermis vel ut. An sit illum euismod facilisis, tamquam vulputate pertinacia eum at.</span><br></p>', '114-FOTY', 'https://prod-mayang.sgp1.digitaloceanspaces.com/img/1184495/1663989196/eyJ3aWR0aCI6NjQwLCJoZWlnaHQiOjY0MCwia2Jfc2l6ZSI6NTA4LjAxNH0%3D', 1, NULL, 1, 4, '2023-06-21 00:30:37', '2023-06-21 00:30:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `types`
--

CREATE TABLE `types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `types`
--

INSERT INTO `types` (`id`, `name`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Daily', 1, NULL, NULL),
(2, 'Weekly', 1, NULL, NULL),
(3, 'Monthly', 1, NULL, NULL),
(4, 'Yearly', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `prices`
--
ALTER TABLE `prices`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `prices`
--
ALTER TABLE `prices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `types`
--
ALTER TABLE `types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
