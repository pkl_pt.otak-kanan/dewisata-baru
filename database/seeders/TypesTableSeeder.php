<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
        $types = [
            ['name' => 'Daily', 'category_id' => 11, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Weekly', 'category_id' => 11, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Monthly', 'category_id' => 11, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Yearly', 'category_id' => 11, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];

        DB::table('types')->insert($types);
    }
}
