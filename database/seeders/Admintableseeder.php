<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class Admintableseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = [
            [
            'name' => 'Hafid',
            'email' => 'admin1@gmail.com',
            'role' => 'admin',
            'password' => bcrypt('hafid'),
            'theme' => 'light',
            'font_size' => 'sedang',
            ],

            [
            'name' => 'Arifin',
            'email' => 'admin2@gmail.com',
            'role' => 'admin',
            'password' => bcrypt('arifin'),
            'theme' => 'light',
            'font_size' => 'sedang',
            ],

            [
            'name' => 'Alya',
            'email' => 'admin3@gmail.com',
            'role' => 'admin',
            'password' => bcrypt('alyatri'),
            'theme' => 'light',
            'font_size' => 'sedang',
            ],

            [
            'name' => 'Prastiwi',
            'email' => 'admin4@gmail.com',
            'role' => 'admin',
            'password' => bcrypt('prastiwi'),
            'theme' => 'light',
            'font_size' => 'sedang',
            ],
    ];


    foreach($admin as $key => $val){
        User::create($val);
    }
    }
}
