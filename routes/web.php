<?php

use Carbon\Carbon;
use App\Models\Invoice;
use App\Models\Product;
use GuzzleHttp\Middleware;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\TypeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\CategoryController;

use App\Http\Controllers\CustomerController;

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\ProductListController;
use App\Http\Controllers\ProfileAdminController;
use App\Http\Controllers\Auth\RegisterController;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $products = Product::orderBy("id","DESC")->get();
    return view('index', compact("products"), [
        'title' => 'home'
    ]);
});
Route::get('/detail', function () {
    return view('detail');
});
Route::get('/list',function(){
    $products = Product::all();
    return view('list', compact('products'), [
        'title' => 'list'
    ]);
});
Route::get('/about',function(){
    return view('about', [
        'title' => 'about'
    ]);
});
Route::get('/contact-us',function(){
    return view('contact', [
        'title' => 'contact'
    ]);
});
Route::get('/histori-pemesanan', [UserController::class, 'histori'])->name('histori.pemesanan');
Route::get('/my-profile', [UserController::class, 'myProfile'])->name('my-profile');
Route::put('/update-my-profile', [UserController::class, 'updateProfile'])->name('update-profile');

Route::get('category/{slug}',[ProductController::class,"showByCategory"]);

Route::group(['prefix'=>'admin', 'middleware' => 'isLogin'], function(){
    Route::get('/dashboard',function(Request $request){
        $now = Carbon::now();
        $year=$now->year;
        if($request->has('year')){
            $year=$request->year;
        }
        $harian = [
            'chart_title' => 'Setiap Hari',
            'report_type' => 'group_by_date',
            'model' => 'App\Models\Invoice',
            'relationship_name'=>'order',
            'where_raw'=>'year(created_at) = '.$year,
            'group_by_field' => 'created_at',
            'group_by_period' => 'day',
            'conditions'            => [
                ['name' => 'Dibayar', 'condition' => 'status = "paid"', 'color' => 'blue', 'fill' => true],
                // ['name' => 'Belum Lunas', 'condition' => 'status = "pending"', 'color' => 'blue', 'fill' => true],
            ],
        
            'aggregate_function' => 'sum',
            // 'filter_field' => 'created_at',
            'aggregate_field' => 'totalAmount',  
            'filter_days' => 30,      
            'chart_type' => 'line',
        ];
        $mingguan = [
            'chart_title' => 'Setiap Minggu',
            'report_type' => 'group_by_date',
            'relationship_name'=>'invoice',
            'model' => 'App\Models\Invoice',
            'group_by_field' => 'created_at',
            'group_by_period' => 'week',
            'where_raw'=>'year(created_at) = '.$year,
            'filter_days' => 30,
            'conditions'            => [
                ['name' => 'Dibayar', 'condition' => 'status = "paid"', 'color' => 'orange', 'fill' => true],
                // ['name' => 'Belum Lunas', 'condition' => 'status = "pending"', 'color' => 'blue', 'fill' => true],
            ],
        
            'aggregate_function' => 'sum',
            // 'filter_field' => 'created_at',
            'aggregate_field' => 'totalAmount',        
            'chart_type' => 'line',
        ];
        $bulanan = [
            'chart_title' => 'Setiap Bulan',
            'report_type' => 'group_by_date',
            'relationship_name'=>'order',
            'model' => 'App\Models\Invoice',
            'group_by_field' => 'created_at',
            'group_by_period' => 'month',
            'where_raw'=>'year(created_at) = '.$year,
            'group_by_field_format' => 'd-m-Y',
            'conditions'            => [
                ['name' => 'Dibayar', 'condition' => 'status = "paid"', 'color' => 'green', 'fill' => true],
                // ['name' => 'Belum Lunas', 'condition' => 'status = "pending"', 'color' => 'blue', 'fill' => true],
            ],
        
            'aggregate_function' => 'sum',
            // 'filter_field' => 'created_at',
            'aggregate_field' => 'totalAmount',        
            'chart_type' => 'line',
        ];
        $chart1 = new LaravelChart($harian,$mingguan,$bulanan);
        
        return view('admin/dashboard',compact('chart1','year'));
    });

    Route::middleware(['guest'])->group(function(){
        Route::get('/login', [LoginController::class, 'index']);
        Route::post('/login', [LoginController::class, 'login_admin']);
    });

    Route::get('/data_admin', [AdminController::class, 'getAdminList'])->name('admin.data_all');
    Route::get('/edit_admin/{id}', [AdminController::class, 'editAdmin'])->name('admin.edit_admin');
    Route::get('/create-admin', [AdminController::class, 'createAdmin'])->name('admin.create_admin');
    Route::post('/create-admin', [AdminController::class, 'create'])->name('admin.create');
    Route::get('/admin/edit_admin/{id}', [AdminController::class, 'editAdmin'])->name('admin.edit_admin');
    Route::patch('/admin/update_admin/{id}', [AdminController::class, 'updateAdmin'])->name('admin.update_admin'); // Metode PATCH untuk update admin
    Route::delete('/admin/delete_admin/{id}', [AdminController::class, 'deleteAdmin'])->name('admin.delete_admin'); // Metode DELETE untuk hapus admin  


    Route::get('/products/edit/{id}', [ProductController::class,'edit'])->name('admin.products.edit');
    Route::get('/categories/edit/{id}', [CategoryController::class,'edit'])->name('admin.categories.edit');
    Route::get('/types/edit/{id}', [TypeController::class,'edit'])->name('admin.types.edit');
    Route::put('/products/update/{id}', [ProductController::class, 'update'])->name('admin.products.update');
    Route::put('/types/update/{id}', [TypeController::class, 'update'])->name('admin.types.update');
    Route::put('/categories/update/{id}', [CategoryController::class, 'update'])->name('admin.categories.update');

    Route::get('/orders', [OrderController::class,'pemesanan'])->name('admin.orders.pemesanan');
    Route::post('/admin/orders/{order}/accept', [OrderController::class,'accept'])->name('admin.orders.accept');
    Route::post('/admin/orders/{order}/reject', [OrderController::class,'reject'])->name('admin.orders.reject');
    Route::post('orders/{order}/complete', [OrderController::class, 'complete'])->name('admin.orders.complete');
    // Route::post('orders/{order}/cancel', [OrderController::class, 'cancel'])->name('admin.orders.cancel');

    Route::post('/admin/orders/{order}/pay', [OrderController::class,'pay'])->name('admin.orders.pay');
    Route::post('/admin/orders/{order}/cancel', [OrderController::class,'cancel'])->name('admin.orders.cancel');

    Route::get('/products', [ProductController::class, 'index'])->name('admin.products.index');
    Route::resource('products', '\App\Http\Controllers\ProductController');
    Route::resource('categories', '\App\Http\Controllers\CategoryController');
    Route::resource('types', '\App\Http\Controllers\TypeController');
    Route::get('/profile', [ProfileAdminController::class, 'profile'])->name('admin.profile');
    Route::get('/customer/{userId}', [OrderController::class, 'customer'])->name('admin.customer');
    Route::get('/all-categories', [CategoryController::class,'allCategories'])->name('admin.categories.all');
    Route::get('admin/categories', [CategoryController::class,'categoryListView'])->name('admin.categories.list');

    Route::get('/admin/settings', [SettingController::class, 'settings'])->name('admin.settings');
    Route::post('/admin/settings/save', [SettingController::class, 'saveSettings'])->name('admin.settings.save');

    // Route::get('/admin/customer', [CustomerController::class, 'users'])->name('admin.users');
    Route::get('/customer', [CustomerController::class, 'users'])->name('admin.users');
    Route::get('/all-product', [ProductListController::class, 'allProducts'])->name('admin.productall');
    Route::get('/productlist', [ProductListController::class, 'productlistView'])->name('admin.productlist');

    Route::get('/admin/laporan-penjualan', [AdminController::class, 'laporanPenjualan'])->name('admin.laporan.penjualan');

});

Route::group(['prefix'=>'admin', 'middleware' => 'isLogin:super_admin'], function(){
    Route::get('/admin/laporan-penjualan', [AdminController::class, 'laporanPenjualan'])->name('admin.laporan.penjualan');
});


Auth::routes();

Route::get('detail/{slug}', [\App\Http\Controllers\ProductController::class,'show'])->name("products.detail");
Route::get('delete.products/{id}', [\App\Http\Controllers\ProductController::class,'destroy'])->name("delete.products");
Route::get('/delete.categories/{id}', [CategoryController::class,'destroy'])->name('delete.categories');
Route::post('update-cart', [App\Http\Controllers\CartController::class, 'updateCart'])->name('cart.update');
Route::get('cart', [App\Http\Controllers\CartController::class, 'cartList'])->name('cart.list');
Route::post('cart', [App\Http\Controllers\CartController::class, 'addToCart'])->name('cart.store');
Route::post('update-cart', [App\Http\Controllers\CartController::class, 'updateCart'])->name('cart.update');
Route::post('remove', [App\Http\Controllers\CartController::class, 'removeCart'])->name('cart.remove');
Route::post('clear', [App\Http\Controllers\CartController::class, 'clearAllCart'])->name('cart.clear');

Route::get('order', [App\Http\Controllers\CartController::class, 'getAllCart'])->name('cart.all');
Route::post('/cart/order', [CartController::class,'createOrder'])->name('cart.order');

Route::get('/completed/{order_id}', [OrderController::class,'completed'])->name('completed');

Route::get('/register',[RegisterController::class, 'register'])->name('register')->middleware('guest');
Route::post('/register',[RegisterController::class, 'create'])->name('request.register');

Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::get('/logout',[LoginController::class, 'logout_admin'])->name('admin.logout');

Route::get('/home', function(){
    return redirect('admin/dashboard');
});
