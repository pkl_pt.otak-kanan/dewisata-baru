@extends('layouts.front')
@section('content')

<style>
    h4{
        text-align: center;
        margin: 20px
    }
</style>

<main>
    <section class="hero_in tours_detail">
        <div class="wrapper">
        </div>
    </section>
    <div class="card mb-3">
        <div class="card-body custom-create-card-body">
            <div class="table-responsive">
                @if($invoices->isEmpty())
                <h4>Maaf, Anda belum mempunyai histori pesanan.</h4>
                @else
                <h4>Histori Pemesanan:</h4>
                <table class="table table-bordered data-table" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Kode Pesanan</th>
                            <th>Produk</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th>Tgl Pemesanan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($invoices as $inv)
                        <tr>
                            <td>{{ $inv->code }}</td>
                            <td>
                                <ul>
                                    @foreach($inv->orders as $key=>$order)
                                    <li>
                                        <a href="{{'detail/'.$order->product->slug}}">{{$key+1}}. {{$order->product->name}} ({{$order->quantity}}x) Rp{{number_format($order->price,0)}} <br>{{$order->product->category->name=='FREE OPEN TRIP'?'Tgl '.$order->booking:''}}</a></li>
                                    @endforeach
                                </ul>
                            </td>
                            {{-- <td>{{ $inv->product->prices->first()->name }}</td> --}}
                            <td>
                                Rp{{number_format($inv->orders->sum('total'),0)}}
                            </td>
                            <td>{{ $inv->status=='pending'?'Menungu Konfirmasi':$inv->status }}</td>
                            <td>{{$inv->created_at->format("d, M Y - h:i:s")}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
    </div>
</main>
@endsection
