<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Ansonika">
  <title>Dewisata - Admin dashboard</title>
	
  <!-- Favicons-->
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
  <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

  <!-- GOOGLE WEB FONT -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	
  <!-- Bootstrap core CSS-->
  <link href="{{url('admin/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Main styles -->
  <link href="{{url('admin/css/admin.css')}}" rel="stylesheet">
  <!-- Icon fonts-->
  <link href="{{url('admin/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <!-- Plugin styles -->
  <link href="{{url('admin/vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
  <!-- Your custom styles -->
  <link href="{{url('admin/css/custom.css')}}" rel="stylesheet">
  <link href="{{url('admin/css/styles.css') }}" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.16.0/font/bootstrap-icons.css" rel="stylesheet">

   @if(auth()->user()->isAdminDarkMode())
    <link href="{{ url('admin/css/dark-mode.css') }}" rel="stylesheet">
  @endif
	
</head>

<body class="fixed-nav sticky-footer{{ auth()->user()->isAdminDarkMode() ? ' dark-mode' : '' }}
  {{ $fontSize === 'besar' ? ' font-besar' : ($fontSize === 'kecil' ? ' font-kecil' : ' font-sedang') }}" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-default fixed-top" id="mainNav">
    <a class="navbar-brand" href="/admin">Dewisata</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{url("/admin/dashboard")}}">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>
      <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Bookings">
            <a class="nav-link" href="{{ route('admin.orders.pemesanan') }}">
                <i class="fa fa-fw fa-calendar-check-o"></i>
                <span class="nav-link-text">Pemesanan 
                    @php
                        $orderCount = app('App\Http\Controllers\OrderController')->getOrderCount();
                    @endphp
                    @if($orderCount > 0)
                        <span class="badge badge-pill badge-primary">{{ $orderCount }} New</span>
                    @endif
                </span>
            </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Master Pengguna">
          <a class="nav-link nav-link-collapse {{ (request()->is('admin/users')) ? '' : 'collapsed' }}" data-toggle="collapse" href="#collapseMasterUsers">
              <i class="fa fa-fw fa-users"></i>
              <span class="nav-link-text">Master Pengguna</span>
          </a>
          <ul class="sidenav-second-level collapse {{ (request()->is('admin/users','admin/users/*')) ? 'show' : '' }}" id="collapseMasterUsers">
              <li>
                  <a href="{{route('admin.users')}}">Data Pelanggan</a>
              </li>
              <li>
                  <a href="{{route ('admin.data_all')}}">Data Admin</a>
              </li>
          </ul>
      </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="kategori">
          <a class="nav-link nav-link-collapse {{ (request()->is('admin/categories')) ? '' : 'collapsed' }}" data-toggle="collapse" href="#collapseKategori">
            <i class="fa fa-fw fa-list-ol"></i>
            <span class="nav-link-text">Layanan</span>
          </a>
          <ul class="sidenav-second-level collapse {{ (request()->is('admin/categories','admin/categories/*')) ? 'show' : '' }}" id="collapseKategori">
            @foreach($categories as $cat)
            <li>
              <a href="{{url('admin/categories?cat='.$cat->slug)}}">{{$cat->name}}</span></a>
            </li>
            @endforeach
            <li>
              {{-- <a href="{{url('admin/categories/create')}}">Tambah Baru</span></a> --}}
            </li>
          </ul>
        </li>
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="My listings">
          <a class="nav-link nav-link-collapse {{ (request()->is('admin/products')) ? '' : 'collapsed' }}" data-toggle="collapse" href="#collapseMylistings">
            <i class="fa fa-fw fa-database"></i>
            <span class="nav-link-text">Produk</span>
          </a>
          <ul class="sidenav-second-level collapse {{ (request()->is('admin/products','admin/products/*')) ? 'show' : '' }}" id="collapseMylistings">
            @foreach($allCategories as $cat)
            <li>
              <a href="{{url('admin/products?cat='.$cat->slug)}}">{{$cat->name}}</span></a>
            </li>
            @endforeach
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="kategori">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseJenisProduk">
            <i class="fa fa-fw fa-random"></i>
            <span class="nav-link-text">Jenis Produk</span>
          </a>
          <ul class="sidenav-second-level collapse {{ (request()->is('admin/types','admin/types/*')) ? 'show' : '' }}" id="collapseJenisProduk">
            @foreach($allCategories as $cat)
            <li>
              <a href="{{url('admin/types?cat='.$cat->slug)}}">{{$cat->name}}</span></a>
            </li>
            @endforeach
            <li>
              <a href="{{url('admin/types/create')}}">Tambah Baru</span></a>
            </li>
          </ul>
        </li>
         @if(Auth::check() && Auth::user()->role == 'super_admin')
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Laporan Penjualan">
            <a class="nav-link" href="{{ route('admin.laporan.penjualan') }}">
                <i class="fa fa-fw fa-bar-chart"></i>
                <span class="nav-link-text">Laporan Penjualan</span>
            </a>
        </li>
         @endif
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="My profile">
          <a class="nav-link" href="{{route('admin.profile')}}">
            <i class="fa fa-fw fa-user"></i>
            <span class="nav-link-text">Profilku</span>
          </a>
        </li>
		    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="My profile">
          <a class="nav-link" href="{{route('admin.settings')}}">
            <i class="fa fa-fw fa-gear"></i>
            <span class="nav-link-text">Settings</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>
  <!-- /Navigation-->
  @yield("content")
    <!-- /.container-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Dewisata</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Apakah Anda yakin ingin Keluar?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Pilih "Keluar" jika ingin mengakhiri sesi</div>
          <div class="modal-footer">
            <form action="" method="GET">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>
            <a class="btn btn-primary" href="{{route('admin.logout')}}">Keluar</a>
          </form>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{url('admin/vendor/jquery/jquery.min.js')}}"></script>

    {{-- <script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> --}}
    <script src="{{url('admin/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script> 
    {{-- <script src="https://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> --}}
    <!-- Core plugin JavaScript-->
    <script src="{{url('admin/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <!-- Page level plugin JavaScript-->

	   <script src="{{url('admin/vendor/jquery.magnific-popup.min.js')}}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{url('admin/js/admin.js')}}"></script>
	@yield("scripts")
</body>
</html>
