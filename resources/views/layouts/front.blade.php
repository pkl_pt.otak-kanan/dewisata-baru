<!DOCTYPE html>
<html lang="en">
<style>
	.logo-txt {
    color: white;
    font-size: 20px;
}
</style>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Panagea - Premium site template for travel agencies, hotels and restaurant listing.">
    <meta name="author" content="Ansonika">
    <title>Dewisata.com</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('css/style.css?sdmmmdd')}}" rel="stylesheet">
	<link href="{{url('css/vendors.css')}}" rel="stylesheet">
	<link href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@6.2.1/css/fontawesome.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

    <!-- YOUR CUSTOM CSS -->
    <link href="{{url('css/custom.css')}}" rel="stylesheet">

</head>

<body class="datepicker_mobile_full">
	<div id="page">
	<header class="header menu_fixed">
		<div id="preloader"><div data-loader="circle-side"></div></div><!-- /Page Preload -->
		<div id="logo">
			<a href="/">
				<span class="logo-txt">INDONESIA DMC</span>
			</a>
		</div>
		<ul id="top_menu">
			<li><a href="/cart" class="cart-menu-btn" title="Cart"><strong>{{Cart::getContent()->count()}}</strong></a></li>
			@auth
				<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
					{{auth()->user()->name}}
				</a>
				<ul class="dropdown-menu" style="background: purple">
				<li>
					<a class="dropdown-item" href="{{route('my-profile')}}">
						<div class="d-flex align-items-center">
							<i class="bi bi-person-circle me-2"></i>
							<span>My Profile</span>
						</div>
					</a>
				</li>
				<li>
					<a class="dropdown-item" href="{{ route('histori.pemesanan') }}">
						<div class="d-flex align-items-center">
							<i class="bi bi-clock-history me-2"></i>
							<span>Histori Pesanan</span>
						</div>
					</a>
				</li>
				<li><hr class="dropdown-divider"></li>
				<li>
					<a class="dropdown-item" href="{{route('admin.logout')}}">
						<div class="d-flex align-items-center" style="margin-left: -12px">
							<i class="bi bi-box-arrow-right me-2"></i>
							<span>Logout</span>
						</div>
					</a>
				</li>
				</ul>
				</li>
			@else
				<li><a href="/login" class="login" title="Sign In">Sign In</a></li>
			@endauth
		</ul>
		<!-- /top_menu -->
		<a href="#menu" class="btn_mobile">
			<div class="hamburger hamburger--spin" id="hamburger">
				<div class="hamburger-box">
					<div class="hamburger-inner"></div>
				</div>
			</div>
		</a>
		<nav id="menu" class="main-menu">
			<ul>
				@if($title??'')
				<li><span><a href="/" class="{{ ($title === "home") ? 'active' : '' }}">Beranda</a></span>
				</li>
                <li><span><a href="#" class="{{ ($title === "list") ? 'active' : '' }}">Produk</a></span>
					<ul>
						@foreach($allCategories as $c)
						<li><a href="{{url("/category/".$c->slug)}}">{{$c->name}}</a></li>
						{{-- @if($c->subcategory->count()>0)
						<ul>
							@foreach($c->subcategory as $s)
							<li>{{$s->name}}</li>
							@endforeach
						</ul>
						@endif --}}
						@endforeach
					</ul>
				</li>
                <li><span><a href="/about" class="{{ ($title === "about") ? 'active' : '' }}">Tentang Kami</a></span>
				</li>
                <li><span><a href="/contact-us" class="{{ ($title === "contact") ? 'active' : '' }}">Hubungi Kami</a></span>
				</li>
				@else
				<li><span><a href="/">Beranda</a></span>
				</li>
                <li><span><a href="#" class="active">Produk</a></span>
					<ul>
						@foreach($allCategories as $c)
						<li><a href="{{url("/category/".$c->slug)}}">{{$c->name}}</a></li>
						{{-- @if($c->subcategory->count()>0)
						<ul>
							@foreach($c->subcategory as $s)
							<li>{{$s->name}}</li>
							@endforeach
						</ul>
						@endif --}}
						@endforeach
					</ul>
				</li>
                <li><span><a href="/about">Tentang Kami</a></span>
				</li>
                <li><span><a href="/contact-us">Hubungi Kami</a></span>
				</li>
				@endif
			</ul>
		</nav>
	</header>
	<!-- /header -->
	@yield("content")

	<footer>
		<div class="container margin_60_35">
			<div class="row">
				<div class="col-lg-5 col-md-12 pe-5">
					<!-- <p><img src="img/logo.svg" width="150" height="36" alt=""></p> -->
					<span class="logo-txt">INDONESIA DMC</span>
					<p>DEWISATA sebagai salah satu Event Organizer yang berfokus pada bidang MICE in Tourism Service dan beroperasional sejak tahun 2009.</p>
					<div class="follow_us">
						<ul>
							<li>Follow us</li>
							<li><a href="#0"><i class="ti-facebook"></i></a></li>
							<li><a href="#0"><i class="ti-twitter-alt"></i></a></li>
							<li><a href="#0"><i class="ti-google"></i></a></li>
							<li><a href="#0"><i class="ti-pinterest"></i></a></li>
							<li><a href="#0"><i class="ti-instagram"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 ms-lg-auto">
					<h5>Useful links</h5>
					<ul class="links">
						<li><a href="/about">About</a></li>
						<li><a href="/login">Login</a></li>
						<li><a href="/register">Register</a></li>
						<li><a href="/contact">Contacts</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-md-6">
					<h5>Contact with Us</h5>
					<ul class="contacts">
						<li><a href="tel://61280932400"><i class="ti-mobile"></i> + 61 23 8093 3400</a></li>
						<li><a href="mailto:info@Panagea.com"><i class="ti-email"></i> info@dewisata.com</a></li>
					</ul>
				</div>
			</div>
			<!--/row-->
			<hr>
			<div class="row">
				<div class="col-lg-12">
					<ul id="additional_links">
						<li><a href="#0">Terms and conditions</a></li>
						<li><a href="#0">Privacy</a></li>
						<li><span>Dewisata.com</span></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<!--/footer-->
	</div>
	<!-- page -->
	
	<!-- Sign In Popup -->
	<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">
		<div class="small-dialog-header">
			<h3>Sign In</h3>
		</div>
		<form>
			<div class="sign-in-wrapper">
				<a href="#0" class="social_bt facebook">Login with Facebook</a>
				<a href="#0" class="social_bt google">Login with Google</a>
				<div class="divider"><span>Or</span></div>
				<div class="form-group">
					<label>Email</label>
					<input type="email" class="form-control" name="email" id="email">
					<i class="icon_mail_alt"></i>
				</div>
				<div class="form-group">
					<label>Password</label>
					<input type="password" class="form-control" name="password" id="password" value="">
					<i class="icon_lock_alt"></i>
				</div>
				<div class="clearfix add_bottom_15">
					<div class="checkboxes float-start">
						<label class="container_check">Remember me
						  <input type="checkbox">
						  <span class="checkmark"></span>
						</label>
					</div>
					<div class="float-end mt-1"><a id="forgot" href="javascript:void(0);">Forgot Password?</a></div>
				</div>
				<div class="text-center"><input type="submit" value="Log In" class="btn_1 full-width"></div>
				<div class="text-center">
					Don’t have an account? <a href="register.html">Sign up</a>
				</div>
				<div id="forgot_pw">
					<div class="form-group">
						<label>Please confirm login email below</label>
						<input type="email" class="form-control" name="email_forgot" id="email_forgot">
						<i class="icon_mail_alt"></i>
					</div>
					<p>You will receive an email containing a link allowing you to reset your password to a new preferred one.</p>
					<div class="text-center"><input type="submit" value="Reset Password" class="btn_1"></div>
				</div>
			</div>
		</form>
		<!--form -->
	</div>
	<!-- /Sign In Popup -->
	
	<div id="toTop"></div><!-- Back to top button -->
	
	<!-- COMMON SCRIPTS -->
    <script src="{{url('js/common_scripts.js')}}"></script>
    <script src="{{url('js/main.js')}}"></script>
	<script src="{{url('phpmailer/validate.js')}}"></script>
	@yield("scripts")
	<!-- DATEPICKER  -->
	<script>
	$(function() {
	  'use strict';
	  $('input[name="dates"]').daterangepicker({
		  autoUpdateInput: false,
		  minDate:new Date(),
		  locale: {
			  cancelLabel: 'Clear'
		  }
	  });
	  $('input[name="dates"]').on('apply.daterangepicker', function(ev, picker) {
		  $(this).val(picker.startDate.format('MM-DD-YY') + ' > ' + picker.endDate.format('MM-DD-YY'));
	  });
	  $('input[name="dates"]').on('cancel.daterangepicker', function(ev, picker) {
		  $(this).val('');
	  });
	});
	</script>
	
	<!-- INPUT QUANTITY  -->
	<script src="{{url('js/input_qty.js')}}"></script>
	
</body>
</html>