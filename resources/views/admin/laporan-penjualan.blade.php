@extends('layouts.admin')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">Laporan Penjualan</h1>
        <div class="card mb-3">
            <div class="card-body custom-create-card-body">
                 <form action="{{ route('admin.laporan.penjualan') }}" method="get" class="form-inline mb-3">
                        <label for="yearFilter" class="mr-2">Filter Tahun:</label>
                        <select name="year" id="yearFilter" class="form-control">
                            @foreach ($years as $year)
                                <option value="{{ $year }}" {{ $year == $yearFilter ? 'selected' : '' }}>{{ $year }}</option>
                            @endforeach
                        </select>
                        <button type="submit" class="btn btn-primary ml-2">Filter</button>
                    </form>

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Bulan</th>
                                <th>Jumlah Pendapatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $totalRevenue = 0;
                            @endphp
                            @foreach (range(1, 12) as $month)
                                @php
                                    $revenue = $monthlyRevenue[$month] ?? 0;
                                    $totalRevenue += $revenue;
                                @endphp
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ date('F', mktime(0, 0, 0, $month, 1)) }}</td>
                                    <td>Rp{{ number_format($revenue, 0) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="2">Total Pendapatan</th>
                                <td>Rp{{ number_format($totalRevenue, 0) }}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            {{-- <div class="card-footer small text-muted">Diperbarui kemarin pukul 23:59</div> --}}
        </div>
        <!-- /tables-->
    </div>
    <!-- /container-fluid-->
</div>
@endsection