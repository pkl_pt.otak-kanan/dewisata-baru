@extends("layouts.admin")
@section("content")

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Master Pengguna</a>
        </li>
		<li class="breadcrumb-item">
			<a href="#">Data Admin</a>
		  </li>
        <li class="breadcrumb-item active">Tambah</li>
      </ol>
	  <form method="post" action="{{ route('admin.create') }}">
		@csrf
		@if (session('success'))
			<div class="alert alert-success" role="alert" data-duration="{{ session('alert-duration', 2000) }}" >
				{{ session('success') }}
			</div>

        @endif
		<script>
			// Cek apakah ada elemen pesan dengan atribut data-duration
			var alertElements = document.querySelectorAll('.alert[data-duration]');
			alertElements.forEach(function(element) {
				var duration = parseInt(element.getAttribute('data-duration'));
				setTimeout(function() {
					element.style.display = 'none';
				}, duration);
			});
		 </script>

		<div class="box_general padding_bottom">
			<div class="header_box version_2">
				<h2><i class="fa fa-file"></i>Admin Baru</h2>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Username</label>
						<input type="text" name="username" class="form-control" placeholder="isi username" required>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Email</label>
						<input type="email" name="email" class="form-control" placeholder="isi email" required>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>Password</label>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Masukkan Password" required>
                        <i class="bi bi-eye-fill"></i>
					</div>
				</div>
			</div>

		<p><button type="submit" class="btn_1 medium">Simpan</button></p>
	  </form>
	  </div>
	  <!-- /.container-fluid-->
   	</div>
@endsection
@section("scripts")
  <!-- WYSIWYG Editor -->
  <link rel="stylesheet" href="{{url('admin/js/editor/summernote-bs4.css')}}">
{{-- <script src="{{url('/admin/vendor/dropzone.min.js')}}"></script> --}}
<!-- WYSIWYG Editor -->
<script src="{{url('/admin/js/editor/summernote-bs4.min.js')}}"></script>
<script>
$('.editor').summernote({
    fontSizes: ['10', '14'],
    toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough']],
        ['fontsize', ['fontsize']],
        ['para', ['ul', 'ol', 'paragraph']]
    ],
    placeholder: 'Write here your description....',
    tabsize: 2,
    height: 200
});
</script>
@stop
