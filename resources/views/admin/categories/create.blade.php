@extends("layouts.admin")
@section("content")
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
		<li class="breadcrumb-item">
			@if($cat)
			<a href="#">Layanan {{$cat->name}}</a>
			@else
			<a href="#">Layanan</a>
			@endif
		  </li>
        <li class="breadcrumb-item active">Tambah baru</li>
      </ol>
      <form method="post" action="{{url('admin/categories')}}">
        @csrf
		<div class="box_general padding_bottom">
			<div class="header_box version_2">
				@if($cat)
				<h2><i class="fa fa-file"></i>Layanan {{$cat->name}} baru</h2>
				@else
				<h2><i class="fa fa-file"></i>Layanan baru</h2>
				@endif				
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Parent</label>
						<div class="styled-select">
						<select name="parent_id">
							@if($cat)
                            <option value="{{$cat->id}}">{{$cat->name}}</option>
							@else
							<option value="">-</option>
							@endif
						</select>
						</div>
					</div>
				</div>
			</div>
			<!-- /row-->
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Judul Kategori</label>
						<input type="text" name="name" class="form-control" placeholder="Isi judul">
					</div>
				</div>
			</div>
			<!-- /row-->
		</div>
		<!-- /box_general-->
		<p><button type="submit" class="btn_1 medium">Simpan</button></p>
      </form>
	  </div>
	  <!-- /.container-fluid-->
   	</div>
@endsection
@section("scripts")
  <!-- WYSIWYG Editor -->
  <link rel="stylesheet" href="{{url('admin/js/editor/summernote-bs4.css')}}">
{{-- <script src="{{url('/admin/vendor/dropzone.min.js')}}"></script> --}}
<!-- WYSIWYG Editor -->
<script src="{{url('/admin/js/editor/summernote-bs4.min.js')}}"></script>
<script>
$('.editor').summernote({
    fontSizes: ['10', '14'],
    toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough']],
        ['fontsize', ['fontsize']],
        ['para', ['ul', 'ol', 'paragraph']]
    ],
    placeholder: 'Write here your description....',
    tabsize: 2,
    height: 200
});
</script>
@stop
