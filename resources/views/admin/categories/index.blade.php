@extends("layouts.admin")
@section("content")
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="#">Layanan</a>
          </li>
        <li class="breadcrumb-item active">{{$cat->name}}</li>
      </ol>
      @if (session('success'))
			<div class="alert alert-success" role="alert" data-duration="{{ session('alert-duration', 2000) }}">
				{{ session('success') }}
			</div>
		  @endif
      <script>
        // Cek apakah ada elemen pesan dengan atribut data-duration
        var alertElements = document.querySelectorAll('.alert[data-duration]');
        alertElements.forEach(function(element) {
            var duration = parseInt(element.getAttribute('data-duration'));
            setTimeout(function() {
                element.style.display = 'none';
            }, duration);
        });
     </script>
		<!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
        <a href="/admin/categories/create?cat={{request()->query('cat') }}"><i class="fa fa-plus"></i> Tambah Baru</a>
        </div>
        <div class="card-body custom-create-card-body">
          <div class="table-responsive">
            <table class="table table-bordered data-table" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nama</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
        {{-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div> --}}
      </div>
	  <!-- /tables-->
	  </div>
	  <!-- /container-fluid-->
   	</div>
@endsection
@section("scripts")
{{-- <link href="https://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"> --}}
{{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"> --}}
    <script src="{{url('admin/vendor/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{url('admin/vendor/datatables/dataTables.bootstrap4.js')}}"></script>
<script type="text/javascript">
    $(function () {
        var cat ="{{request()->query('cat')}}";
      var table = $("#dataTable").DataTable({
          processing: true,
          serverSide: true,
          ajax: "{{ url('admin/categories?cat=') }}"+cat,
          columns: [
              {data: 'id', name: 'id'},
              {data: 'name', name: 'name'},
              {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });
      
    });
  </script>
@stop