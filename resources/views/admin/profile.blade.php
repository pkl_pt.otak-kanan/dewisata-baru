@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-header">
                        <h3>Profil Pengguna</h3>
                    </div>
                    <div class="card-body custom-create-card-body">
                        <p><strong>Nama:</strong> {{ $user->name }}</p>
                        <p><strong>Email:</strong> {{ $user->email }}</p>
                        <!-- Informasi lainnya yang ingin ditampilkan -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection