@extends("layouts.admin")
@section("content")
<div class="content-wrapper dark-mode" style=" font-family: Arial, sans-serif;">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <div class="container">
            <h3>Pengaturan</h3>
            <form action="{{ route('admin.settings.save') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="darkMode">Mode Tampilan:</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="mode" id="modeGelap" value="dark" {{ $darkMode ? 'checked' : '' }}>
                    <label class="form-check-label" for="modeGelap">
                        Gelap
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="mode" id="modeTerang" value="light" {{ $darkMode ? '' : 'checked' }}>
                    <label class="form-check-label" for="modeTerang">
                        Terang
                    </label>
                </div>
            </div>
            <div class="form-group">
              <label for="fontSize">Ukuran Font:</label>
              <select id="fontSize" name="fontSize">
                <option value="kecil" {{ $fontSize === 'kecil' ? 'selected' : '' }}>Kecil</option>
                <option value="sedang" {{ $fontSize === 'sedang' ? 'selected' : '' }}>Sedang</option>
                <option value="besar" {{ $fontSize === 'besar' ? 'selected' : '' }}>Besar</option>
              </select>
            </div>

            <button type="submit" class="btn btn-primary">Simpan</button>
            <script>
             document.getElementById('fontSize').addEventListener('change', function() {
              var selectedFontSize = this.value;
              var body = document.querySelector('body');

              if (selectedFontSize === 'kecil') {
                body.classList.add('font-kecil');
                body.classList.remove('font-sedang', 'font-besar');
              } else if (selectedFontSize === 'sedang') {
                body.classList.remove('font-kecil', 'font-besar');
                body.classList.add('font-sedang');
              } else if (selectedFontSize === 'besar') {
                body.classList.remove('font-kecil', 'font-sedang');
                body.classList.add('font-besar');
              }
            });

            // Setel ukuran font saat halaman dimuat
            var savedFontSize = "{{ $fontSize }}";
            var body = document.querySelector('body');

            if (savedFontSize === 'kecil') {
              body.classList.add('font-kecil');
            } else if (savedFontSize === 'sedang') {
              body.classList.add('font-sedang');
            } else if (savedFontSize === 'besar') {
              body.classList.add('font-besar');
            }

            // Periksa mode terang atau gelap
            var savedDarkMode = "{{ $darkMode }}";

            if (!savedDarkMode) {
              var fontSizeSelect = document.getElementById('fontSize');
              var selectedFontSize = fontSizeSelect.options[fontSizeSelect.selectedIndex].value;
              var body = document.querySelector('body');

              if (selectedFontSize === 'kecil') {
                body.classList.add('font-kecil');
                body.classList.remove('font-sedang', 'font-besar');
              } else if (selectedFontSize === 'sedang') {
                body.classList.remove('font-kecil', 'font-besar');
                body.classList.add('font-sedang');
              } else if (selectedFontSize === 'besar') {
                body.classList.remove('font-kecil', 'font-sedang');
                body.classList.add('font-besar');
              }
            } else {
              var fontSizeSelect = document.getElementById('fontSize');
              fontSizeSelect.value = savedFontSize;
            }
            </script>
          </form>
          </div>

@endsection
