@extends("layouts.admin")
@section("content")
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="card mb-3">
            <div class="card-body custom-create-card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama Layanan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            {{-- <div class="card-footer small text-muted">Diperbarui kemarin pukul 23:59</div> --}}
        </div>
        <!-- /tables-->
    </div>
    <!-- /container-fluid-->
</div>
@endsection

@section('scripts')
<link rel="stylesheet" href="{{ asset('admin/vendor/datatables/dataTables.bootstrap4.css') }}">
<script src="{{ asset('admin/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('admin/vendor/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('admin/vendor/datatables/dataTables.bootstrap4.js') }}"></script>

<script>
    $(document).ready(function () {
        var table = $("#dataTable").DataTable({
            serverSide: true,
            ajax: "{{ route('admin.categories.all') }}",
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
        });
    });
</script>
@endsection
