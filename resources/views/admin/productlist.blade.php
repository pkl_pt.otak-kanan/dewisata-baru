@extends("layouts.admin")
@section("content")
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#">Layanan</a>
            </li>
            <li class="breadcrumb-item active">{{ isset($cat) ? $cat->name : '' }}</li>
        </ol>
        @if (session('success'))
        <div class="alert alert-success" role="alert" data-duration="{{ session('alert-duration', 2000) }}">
            {{ session('success') }}
        </div>
        @endif
        <script>
            // Cek apakah ada elemen pesan dengan atribut data-duration
            var alertElements = document.querySelectorAll('.alert[data-duration]');
            alertElements.forEach(function(element) {
                var duration = parseInt(element.getAttribute('data-duration'));
                setTimeout(function() {
                    element.style.display = 'none';
                }, duration);
            });
        </script>
        <!-- Example DataTables Card-->
        <div class="card mb-3">
            <div class="card-body custom-create-card-body">
                <div class="table-responsive">
                    <table class="table table-bordered data-table" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama Produk</th>
                                <th>Jenis Produk</th>
                                <th>Kuota</th>
                                <th>Gambar</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- Data tabel akan diisi oleh JavaScript menggunakan DataTables --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("scripts")
<script src="{{url('admin/vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{url('admin/vendor/datatables/dataTables.bootstrap4.js')}}"></script>
<script type="text/javascript">
    $(function () {
        var params = new window.URLSearchParams(window.location.search);
        catname = params.get('cat');
        var table = $("#dataTable").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('admin.productall') }}',
                data: function (d) {
                    d.cat = catname;
                },
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'jenis_produk', name: 'jenis_produk'},
                {
                    data: 'kuota',
                    name: 'kuota',
                    render: function (data, type, row) {
                        if (row.category_name === 'FREE OPEN TRIP') {
                            return data === 0 ? 'Habis' : data;
                        } else {
                            return data === 0 ? 'Unlimited' : data;
                        }
                    }
                },
                {data: 'image', name:'image'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        // Handle edit button click
        $('#dataTable').on('click', '.edit', function () {
            var data = table.row($(this).closest('tr')).data();
            var productId = data.id;
            // Redirect ke halaman edit dengan ID produk
            window.location.href = "/admin/products/" + productId + "/edit";
        });
    });
</script>
@stop
