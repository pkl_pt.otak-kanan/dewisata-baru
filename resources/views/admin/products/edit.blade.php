@extends("layouts.admin")
@section("content")
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
		<li class="breadcrumb-item">
<<<<<<< HEAD
			<a href="#">Produk</a>
		  </li>
        <li class="breadcrumb-item">{{$cat->name}}</li>
		<li class="breadcrumb-item active">
			Edit
=======
            <a href="#">Produk</a>
        </li>
      <li class="breadcrumb-item">{{$cat->name}}</li>
      <li class="breadcrumb-item active">
          Edit
>>>>>>> c811b39fe37afbcca0728f37eb328be4e19c0d21
		  </li>
      </ol>
	  <form method="post" enctype="multipart/form-data" action="{{ route('admin.products.update', ['id' => $product->id]) }}">
		@csrf
        @method('put')

		<div class="box_general padding_bottom">
			<div class="header_box version_2">
				<h2><i class="fa fa-file"></i>Basic info</h2>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Judul</label>
						<input type="text" name="name" class="form-control" placeholder="isi judul" value="{{ $product->name }}">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Kategori</label>
						<div class="styled-select">
						<select name="category_id">
							{{-- @foreach($cat->subcategory as $category)
                                <option value="{{ $category->id }}" {{ $product->category_id == $category->id ? 'selected' : '' }}>
                                {{ $category->name }}
                                </option>
                            @endforeach --}}
							<option value="{{$cat->id}}" selected>{{$cat->name}}</option>
						</select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Jenis Produk</label>
						<div class="styled-select">
						<select name="type_id" >
							{{-- <option value="">Pilih Jenis</option> --}}
                            @foreach($types as $type)
                                <option value="{{ $type->id }}" {{ $product->type_id == $type->id ? 'selected' : '' }}>
                                {{ $type->name }}
                                </option>
                            @endforeach
						</select>

						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>PAX</label>
						<div class="styled-select">
						<select name="group_id">
							<option value="" disabled selected>Pilih PAX</option>
                            @foreach($groups as $c)
							<option value="{{$cat->id}}" {{ $product->group_id == $cat->id ? 'selected' : '' }}>{{$c->name}}</option>
                            @endforeach
						</select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Kode Produk</label>
						<div class="styled-select">
							<input type="text" name="code" class="form-control" placeholder="isi kode produk" value="{{ $product->code }}">
						</div>
					</div>
				</div>
			</div>
			<!-- /row-->
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Gambar</label>
						<img src="{{url("/images/".$product->image)}}" width="50" height="50">
						<input type="file" name="image" class="form-control">
					</div>
				</div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kuota</label>
                    <div class="styled-select">
                        <input type="text" name="kuota" class="form-control" placeholder="isi kuota produk"
						value="{{$product->kuota}}">
                    </div>
                </div>
            </div>
		</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Description</label>
						<textarea name="description" class="editor" value="{{ $product->description }}">{{ $description }}</textarea>
					</div>
				</div>
			</div>
			<!-- /row-->
		</div>
		<!-- /box_general-->
		<!-- Isian harga -->
            <div class="box_general padding_bottom">
                <div class="header_box version_2">
                    <h2><i class="fa fa-money"></i>Harga</h2>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h6>Item</h6>
                        <table id="pricing-list-container" style="width:100%;">
                            @foreach($product->prices as $price)
                                <tr class="pricing-list-item">
                                    <td>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" name="label[]" class="form-control" value="{{ $price->name }}" placeholder="Nama item">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="text" name="price[]" class="form-control" value="{{ $price->price }}" placeholder="Rp0">
                                                </div>
                                            </div>
                                            <div class="col-md-2 d-flex align-items-center">
                                                <div class="form-group">
                                                    <a class="delete" href="#"><i class="fa fa-fw fa-remove"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <a href="#0" class="btn_1 gray add-pricing-list-item"><i class="fa fa-fw fa-plus-circle"></i>Tambah Item</a>
                    </div>
                </div>
            </div>

            <p><button type="submit" class="btn_1 medium">Update</button></p>
        </form>
    </div>
    <!-- /.container-fluid-->
</div>
@endsection

@section("scripts")
<!-- WYSIWYG Editor -->
<link rel="stylesheet" href="{{ url('admin/js/editor/summernote-bs4.css') }}">
<script src="{{ url('/admin/js/editor/summernote-bs4.min.js') }}"></script>
<script>
    $('.editor').summernote({
        fontSizes: ['10', '14'],
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']]
        ],
        placeholder: 'Write here your description....',
        tabsize: 2,
        height: 200
    });
</script>
@stop
