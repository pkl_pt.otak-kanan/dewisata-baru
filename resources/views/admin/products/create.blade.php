@extends("layouts.admin")
@section("content")
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
		<li class="breadcrumb-item">
			<a href="#">Produk {{$cat->name}}</a>
		  </li>
        <li class="breadcrumb-item active">Tambah Baru</li>
      </ol>
	  <form method="post" action="{{url("admin/products")}}" enctype="multipart/form-data">
		@csrf
		@if (session('success'))
			<div class="alert alert-success" role="alert" data-duration="{{ session('alert-duration', 2000) }}" >
				{{ session('success') }}
			</div>

        @endif
		<script>
			// Cek apakah ada elemen pesan dengan atribut data-duration
			var alertElements = document.querySelectorAll('.alert[data-duration]');
			alertElements.forEach(function(element) {
				var duration = parseInt(element.getAttribute('data-duration'));
				setTimeout(function() {
					element.style.display = 'none';
				}, duration);
			});
		 </script>

		<div class="box_general padding_bottom">
			<div class="header_box version_2">
				<h2><i class="fa fa-file"></i>Produk {{$cat->name}} baru</h2>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Judul</label>
						<input type="text" name="name" class="form-control" placeholder="isi judul">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Kategori</label>
						<div class="styled-select">
						<select name="category_id">
							<option value="">Pilih Kategori</option>
							{{-- @foreach($cat->subcategory as $cat) --}}
                            <option value="{{$cat->id}}" selected>{{$cat->name}}</option>
							{{-- @endforeach --}}
						</select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Jenis Produk</label>
						<div class="styled-select">
						<select name="type_id">
							<option value="">Pilih Jenis</option>
                            @foreach($types as $t)
							<option value="{{$t->id}}">{{$t->name}}</option>
                            @endforeach
						</select>

						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>PAX</label>
						<div class="styled-select">
						<select name="group_id">
							<option value="" disabled selected>Pilih PAX</option>
                            @foreach($groups as $c)
							<option value="{{$cat->id}}">{{$c->name}}</option>
                            @endforeach
						</select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Kode Produk</label>
						<div class="styled-select">
							<input type="text" name="code" class="form-control" placeholder="isi kode produk">
						</div>
					</div>
				</div>

			</div>
			<!-- /row-->
		<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Gambar</label>
						<input type="file" name="image" class="form-control">
					</div>
				</div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kuota</label>
                    <div class="styled-select">
                        <input type="text" name="kuota" class="form-control" placeholder="isi kuota produk">
                    </div>
                </div>
            </div>
		</div>

			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Description</label>
						<textarea name="description" class="editor"></textarea>
					</div>
				</div>
			</div>
			<!-- /row-->
		</div>
		<!-- /box_general-->
		<div class="box_general padding_bottom">
			<div class="header_box version_2">
				<h2><i class="fa fa-money"></i>Harga</h2>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h6>Item</h6>
					<table id="pricing-list-container" style="width:100%;">
						<tr class="pricing-list-item">
							<td>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<input type="text" name="label[]" class="form-control" placeholder="Nama item">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<input type="text" name="price[]" class="form-control"  placeholder="Rp0">
										</div>
									</div>
									<div class="col-md-2 d-flex align-items-center">
										<div class="form-group">
											<a class="delete" href="#"><i class="fa fa-fw fa-remove"></i></a>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
					<a href="#0" class="btn_1 gray add-pricing-list-item"><i class="fa fa-fw fa-plus-circle"></i>Tambah Item</a>
					</div>
			</div>
			<!-- /row-->
		</div>
		<!-- /box_general-->
		<p><button type="submit" class="btn_1 medium">Simpan</button></p>
	  </form>
	  </div>
	  <!-- /.container-fluid-->
   	</div>
@endsection
@section("scripts")
  <!-- WYSIWYG Editor -->
  <link rel="stylesheet" href="{{url('admin/js/editor/summernote-bs4.css')}}">
{{-- <script src="{{url('/admin/vendor/dropzone.min.js')}}"></script> --}}
<!-- WYSIWYG Editor -->
<script src="{{url('/admin/js/editor/summernote-bs4.min.js')}}"></script>
<script>
$('.editor').summernote({
    fontSizes: ['10', '14'],
    toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough']],
        ['fontsize', ['fontsize']],
        ['para', ['ul', 'ol', 'paragraph']]
    ],
    placeholder: 'Write here your description....',
    tabsize: 2,
    height: 200
});
</script>
@stop
