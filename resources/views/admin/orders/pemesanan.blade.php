@extends("layouts.admin")
@section("content")
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="card mb-3">
            <div class="card-body custom-create-card-body">
                <div class="table-responsive">
                    @if(session('success'))
                    <div class="alert alert-success" data-duration="{{ session('alert-duration', 2000) }}">
                        {{ session('success') }}
                    </div>
                    @endif
                    @if(session('danger'))
                    <div class="alert alert-danger" data-duration="{{ session('alert-duration', 2000) }}">
                        {{ session('danger') }}
                    </div>
                    @endif

                    <script>
                        var alertElements = document.querySelectorAll('.alert[data-duration]');
                        alertElements.forEach(function(element) {
                            var duration = parseInt(element.getAttribute('data-duration'));
                            setTimeout(function() {
                                element.style.display = 'none';
                            }, duration);
                        });
                    </script>
                    <table class="table table-bordered data-table" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Pelanggan</th>
                                <th>Produk</th>
                                <th>Jumlah</th>
                                <th>Status</th>
                                <th>Tgl Pesanan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            {{-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div> --}}
        </div>
        <!-- /tables-->
    </div>
    <!-- /container-fluid-->
</div>
@endsection
@section("scripts")
<link rel="stylesheet" href="{{url('admin/vendor/datatables/dataTables.bootstrap4.css')}}">
<script src="{{url('admin/vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{url('admin/vendor/datatables/dataTables.bootstrap4.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        var table = $("#dataTable").DataTable({
            serverSide: true,
            ajax: "{{ route('admin.orders.pemesanan') }}",
            order: [[0, 'desc']],
            columns: [
                {data: 'code', name: 'code', orderable: true},
                {data: 'customer', name: 'customer', orderable: true},
                {data: 'product', name: 'product', orderable: true},
                {data: 'total', name: 'total'},
                {data: 'status', name: 'status', orderable: true},
                {data:'created_at',name:'created_at',orderable:true},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            drawCallback: function () {
                // Tangani klik tombol "Dibayar" dan "Batalkan"
                $('.btn-aksi').click(function (e) {
                    e.preventDefault();
                    var url = $(this).attr('data-action');
                    var csrf = '{{ csrf_token() }}';

                    // Tambahkan form yang akan di-submit ke URL aksi
                    var form = $('<form method="POST" action="' + url + '"></form>');
                    form.append('<input type="hidden" name="_token" value="' + csrf + '">');
                    form.append('<input type="hidden" name="_method" value="POST">');

                    // Tambahkan form ke dalam body dan submit form
                    $(document.body).append(form);
                    form.submit();
                });
            }
        });
    });
</script>

@endsection
