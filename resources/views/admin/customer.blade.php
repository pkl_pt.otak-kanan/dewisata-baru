@extends("layouts.admin")
@section("content")
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="card mb-3">
            <div class="card-header">
            <h3>Data Pembeli</h3>
            </div>
        @if (session('success'))
        <div class="alert alert-success" role="alert" data-duration="{{ session('alert-duration', 2000) }}">
            {{ session('success') }}
        </div>
        @endif
        <script>
            // Cek apakah ada elemen pesan dengan atribut data-duration
            var alertElements = document.querySelectorAll('.alert[data-duration]');
            alertElements.forEach(function(element) {
                var duration = parseInt(element.getAttribute('data-duration'));
                setTimeout(function() {
                    element.style.display = 'none';
                }, duration);
            });
        </script>
        <div class="card-body custom-create-card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="customer-table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th colspan="2">Pelanggan</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>ID</th>
                        <td>{{ $user->id }}</td>
                    </tr>
                    <tr>
                        <th>Username</th>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <th>No. WhatsApp</th>
                        <td>{{ $user->whatsapp }}</td>
                    </tr>
                    <tr>
                        <th>Role</th>
                        <td>{{ $user->role }}</td>
                    </tr>
                </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
    <!-- /tables-->
</div>
<!-- /container-fluid-->
@endsection
