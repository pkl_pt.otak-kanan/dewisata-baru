@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="card mb-3">
                <div class="card-header">
                <h3>Data Pelanggan</h3>
                </div>
            <div class="card-body custom-create-card-body">
                <div class="table-responsive">
                    <table class="table table-bordered data-table" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th width="5%">No.</th>
                                <th width="35%">Nama</th>
                                <th width="30%">Email</th>
                                <th width="30%">No. Whatsapp</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no=1; @endphp
                            @foreach($pengguna as $u)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $u->name }}</td>
                                <td>{{ $u->email }}</td>
                                <td>{{ $u->whatsapp }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section("scripts")
{{-- <link href="https://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"> --}}
{{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"> --}}
    <script src="{{url('admin/vendor/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{url('admin/vendor/datatables/dataTables.bootstrap4.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#dataTable').DataTable();
        })
  </script>
@endsection