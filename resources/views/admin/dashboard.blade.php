@extends('layouts.admin')
@section("content")
<div class="content-wrapper">
    <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="dashboard">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">My Dashboard</li>
    </ol>
    <!-- Icon Cards-->
    <div class="row">
      <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card dashboard text-white bg-primary o-hidden h-100">
          <div class="card-body">
            <div class="card-body-icon">
              <i class="fa fa-fw fa-envelope-open"></i>
            </div>
            @php
              $orderCount = app('App\Http\Controllers\OrderController')->getOrderCount();
            @endphp
            <div class="mr-5"><h5>{{ $orderCount }} Pemesanan!</h5></div>
          </div>
          <a class="card-footer text-white clearfix small z-1" href="{{ route('admin.orders.pemesanan') }}">
            <span class="float-left">Lihat Detail</span>
            <span class="float-right">
              <i class="fa fa-angle-right"></i>
            </span>
          </a>
        </div>
      </div>
      <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card dashboard text-white bg-warning o-hidden h-100">
          <div class="card-body">
            <div class="card-body-icon">
              <i class="fa fa-fw fa-users"></i>
            </div>
            @php
              $usersCount = app('App\Http\Controllers\CustomerController')->getUsersCount();
            @endphp
              <div class="mr-5"><h5>{{ $usersCount }} Data Pelanggan!</h5></div>
          </div>
          <a class="card-footer text-white clearfix small z-1" href="{{ route('admin.users') }}">
            <span class="float-left">Lihat Detail</span>
            <span class="float-right">
              <i class="fa fa-angle-right"></i>
            </span>
          </a>
        </div>
      </div>
      <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card dashboard text-white bg-success o-hidden h-100">
          <div class="card-body">
            <div class="card-body-icon">
              <i class="fa fa-fw fa-list-ol"></i>
            </div>
             @php
              $categoryCount = app('App\Http\Controllers\CategoryController')->getCategoryCount();
            @endphp
              <div class="mr-5"><h5>{{ $categoryCount }} Layanan!</h5></div>
          </div>
          <a class="card-footer text-white clearfix small z-1" href="{{route('admin.categories.list')}}">
            <span class="float-left">Lihat Detail</span>
            <span class="float-right">
              <i class="fa fa-angle-right"></i>
            </span>
          </a>
        </div>
      </div>
      <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card dashboard text-white bg-danger o-hidden h-100">
          <div class="card-body">
            <div class="card-body-icon">
              <i class="fa fa-fw fa-database"></i>
            </div>
            @php
              $productsCount = app('App\Http\Controllers\ProductListController')->getProductsCount();
            @endphp
            <div class="mr-5"><h5>{{ $productsCount }} Produk!</h5></div>
          </div>
          <a class="card-footer text-white clearfix small z-1" href="{{ route('admin.productlist') }}">
            <span class="float-left">Lihat Detail</span>
            <span class="float-right">
              <i class="fa fa-angle-right"></i>
            </span>
          </a>
        </div>
      </div>
      </div>
      <!-- /cards -->
      <h2></h2>
       @if(Auth::check() && Auth::user()->role == 'super_admin')
      <div class="box_general padding_bottom">
          <div class="header_box version_2">
            <h2><i class="fa fa-bar-chart"></i>Laporan Pendapatan</h2>
            <select class="form-control">
              @for($i=2020;$i<2040;$i++)
              <option value="{{$i}}" {{$i==$year?'selected':''}}>{{$i}}</option>
              @endfor
            </select>
          </div>
          {!! $chart1->renderHtml() !!}
          {{-- <canvas id="myAreaChart" width="100%" height="30" style="margin:45px 0 15px 0;"></canvas> --}}
      </div>
      @endif
    </div>
    <!-- /.container-fluid-->
     </div>
</div>
@endsection
@section('scripts')
{!! $chart1->renderChartJsLibrary() !!}
{!! $chart1->renderJs() !!}
@endsection
