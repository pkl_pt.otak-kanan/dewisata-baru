@extends("layouts.app")
@section('content')
<style>
    .password-toggle {
        position: relative;
    }

    .password-toggle .toggle-password {
        position: absolute;
        top: 50%;
        right: 10px;
        transform: translateY(-50%);
        cursor: pointer;
    }
</style>
    <main>
        <div class="container">
            <div id="login" class="login-form">
                <figure class="mt-1">
                    <a href="/"><img src="img/logo_sticky.svg" width="155" height="36" alt="" class="logo_sticky"></a>
                </figure>
                @if (session('message')=='berhasil')
                    <div class="allert allert-success">
                        Berhasil Register ! Silahkan Login 
                    </div>
                @endif
               @if(session('logout_success'))
                    <div class="alert alert-success" data-duration="{{ session('alert-duration', 2000) }}">
                        {{ session('logout_success') }}       
                    </div>
                @endif

                @if(session('errorlogin'))
                    <div class="alert alert-danger" data-duration="{{ session('alert-duration', 2000) }}">
                        {{ session('errorlogin') }}
                    </div>
                @endif

                <script>
                    // Cek apakah ada elemen pesan dengan atribut data-duration
                    var alertElements = document.querySelectorAll('.alert[data-duration]');
                    alertElements.forEach(function(element) {
                        var duration = parseInt(element.getAttribute('data-duration'));
                        setTimeout(function() {
                            element.style.display = 'none';
                        }, duration);
                    });
                </script>
                <form action="" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Email</label>
                        <div>
                            @error('email')
                                <small style="color: red">{{$message}}</small>
                            @enderror
                        </div>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Masukkan Email" value="{{ $_COOKIE['email'] ?? '' }}">
                        <i class="icon_mail_alt"></i>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <div>
                            @error('password')
                                <small style="color: red">{{$message}}</small>
                            @enderror
                        </div>
                        <div class="password-toggle">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Masukkan Password" value="{{ $_COOKIE['password'] ?? '' }}">
                            <i class="icon_lock_alt" style="margin-top: -10px"></i>
                            <i class="toggle-password icon-eye-off"></i> <!-- Tambahkan ikon mata (eye) -->
                        </div>
                    </div>
                    <div class="clearfix add_bottom_30">
                        <div class="checkboxes float-start">
                            <label class="container_check" for="remember">Ingat Aku
                            <input type="checkbox" name="remember" id="remember"
                            @if(isset($_COOKIES["email"])) checked="" @endif>
                            <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="float-end mt-1"><a id="forgot" href="{{ route('password.request') }}">Lupa Password?</a></div>
                    </div>
                    <button class="btn_1 rounded full-width" type="submit" name="submit" >Masuk</button>
                    <div class="text-center add_top_10">Belum punya akun? <strong><a href="/register">Daftar!</a></strong></div>
                </form>
            </div>
            
        </div>
    </main>
	<!-- /login -->
		
	<!-- COMMON SCRIPTS -->
    <script src="js/common_scripts.js"></script>
    <script src="js/main.js"></script>
	<script src="phpmailer/validate.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var togglePassword = document.querySelector('.toggle-password');
            var passwordInput = document.getElementById('password');
    
            togglePassword.addEventListener('click', function() {
                if (passwordInput.type === 'password') {
                    passwordInput.type = 'text';
                     togglePassword.classList.remove('icon-eye-off');
                    togglePassword.classList.add('icon-eye');
                } else {
                    passwordInput.type = 'password';
                    togglePassword.classList.remove('icon-eye');
                    togglePassword.classList.add('icon-eye-off');
                   
                }
            });
        });
    </script>	

@endsection
