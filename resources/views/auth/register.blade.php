@extends("layouts.app")
@section('content')

<style>
    .password-toggle {
        position: relative;
    }
	.icon-wrapper {
        position: relative;
    }

    .icon-wrapper .icon {
        position: absolute;
        top: 70%;
        left: 10px;
        transform: translateY(-50%);
    }
	.icon-wrapper .form-control {
        padding-left: 30px;
    }

    .password-toggle .toggle-password {
        position: absolute;
        top: 50%;
        right: 10px;
        transform: translateY(-50%);
        cursor: pointer;
    }
</style>

<main>
    <div class="container">
        <div id="login" class="login-form">
			<figure class="mt-1">
				<a href="/"><img src="img/logo_sticky.svg" width="155" height="36" alt="" class="logo_sticky"></a>
			</figure>
			<form method="POST" action="{{route('request.register')}}">
				@csrf
				<div class="form-group">
					<label>Username</label>
					<div>
						@error('name')
							<small style="color: red">{{$message}}</small>
						@enderror
					</div>
					<input class="form-control" type="text" name="name" placeholder="Masukan Username">
					<i class="ti-user"></i>
				</div>
				<div class="form-group">
					<label>Email</label>
					<div>
						@error('email')
							<small style="color: red">{{$message}}</small>
						@enderror
					</div>
					<input class="form-control" type="email" name="email" placeholder="Masukkan Email">
					<i class="icon_mail_alt"></i>
				</div>
				<div class="form-group icon-wrapper">
                    <label>Nomor WhatsApp</label>
                    <div>
                        @error('whatsapp')
                        <small style="color: red">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="icon">
                        <i class="bi bi-whatsapp"></i>
                    </div>
                    <input class="form-control" type="number" name="whatsapp" placeholder="Masukkan Nomor WhatsApp">
                </div>

                <div class="form-group icon-wrapper">
                    <label>Alamat</label>
                    <div>
                        @error('address')
                        <small style="color: red">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="icon">
                        <i class="bi bi-house"></i>
                    </div>
                    <input class="form-control" type="text" name="alamat" placeholder="Masukkan Alamat">
                </div>
				<div class="form-group">
					<label>Password</label>
					<div>
						@error('password')
							<small style="color: red">{{$message}}</small>
						@enderror
					</div>
					<div class="password-toggle">
						<input type="password" class="form-control" name="password" id="password" placeholder="Masukkan Password" value="{{ $_COOKIE['password'] ?? '' }}">
						<i class="icon_lock_alt" style="margin-top: -10px"></i>
						<i class="toggle-password icon-eye-off"></i> 
					</div>
				</div>
				<div class="form-group">
					<label>Konfirmasi Password</label>
					<div>
						@error('password')
							<small style="color: red">{{$message}}</small>
						@enderror
					</div>
					<div class="password-toggle">
						<input type="password" class="form-control" name="password" id="password" placeholder="Masukkan Password" value="{{ $_COOKIE['password'] ?? '' }}">
						<i class="icon_lock_alt" style="margin-top: -10px"></i>
						<i class="toggle-password icon-eye-off"></i> 
					</div>
				</div>
				{{-- <div id="pass-info" class="clearfix"></div> --}}
				<button class="btn_1 rounded full-width add_top_30" type="submit">Daftar</button>
				{{-- <div class="text-center add_top_10">Already have an acccount? <strong><a href="/login">Sign In</a></strong></div> --}}
			</form>
			
	    </div>
	<!-- /login -->
    </div>
</main>
	
	<!-- COMMON SCRIPTS -->
    <script src="js/common_scripts.js"></script>
    <script src="js/main.js"></script>
	<script src="phpmailer/validate.js"></script>
	<script>
	document.addEventListener('DOMContentLoaded', function() {
		var togglePassword = document.querySelector('.toggle-password');
		var passwordInput = document.getElementById('password');

		togglePassword.addEventListener('click', function() {
			if (passwordInput.type === 'password') {
				passwordInput.type = 'text';
				togglePassword.classList.remove('icon-eye-off');
				togglePassword.classList.add('icon-eye');
			} else {
				passwordInput.type = 'password';
				togglePassword.classList.remove('icon-eye');
				togglePassword.classList.add('icon-eye-off');
			}
		});
	});
	</script>	
	
	<!-- SPECIFIC SCRIPTS -->
	<script src="js/pw_strenght.js"></script>

@endsection
