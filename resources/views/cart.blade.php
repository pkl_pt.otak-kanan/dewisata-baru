@extends("layouts.front")
@section("content")
<main>
    <section class="hero_in tours_detail">
        <div class="wrapper">
        </div>
    </section>
    <!--/hero_in-->
	@if(session('error'))
	<div class="alert alert-danger" data-duration="{{ session('alert-duration', 2000) }}">
		{{ session('error') }}
	</div>
	@endif
	<div class="alert alert-danger" data-duration="2000" style="display: none;" id="error-alert"></div>

	<div class="bg_color_1">
		<div class="container margin_60_35" style="transform: none;">
			<div class="row" style="transform: none;">
                <h4 style="color: #340034">Keranjang Belanja</h4>
				<hr>
				<div class="col-lg-8">
					<div class="box_cart">
					<table class="table cart-list">
						<thead>
							<tr>
								<th>Item</th>
								<th>Harga</th>
								<th>Jumlah</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							@foreach($cartItems as $item)
							<form action="{{ route('cart.update') }}" method="POST" id="cart-order-form">
								@csrf
								<input type="hidden" name="id" value="{{ $item->id}}" >
							<tr>
								<td>
									 <strong>{{ $item->name }}</strong><br>
										@if ($item['category_name'] === 'FREE OPEN TRIP')
                                            @if ($item['kuota'] < 0)
                                            <span id="kuota_id{{$item->id}}">Kuota Hari Ini: <strong style="color: red">Sudah Habis!</strong></span>
                                        @else
                                        <span id="kuota_id{{$item->id}}">Kuota tersisa: {{ $item['kuota']}}</span>
                                        @endif
                                    @else
                                    {{-- <span id="kuota_id{{$item->id}}">Kuota tersisa: <strong style="color: green">{{ $item['kuota'] == 0 ? 'Unlimited' : $item->kuota }}</strong></span> --}}
                                    @endif
            					</td>
								<td>
									<strong>Rp{{number_format($item->price,0)}}</strong>
								</td>
								<td>
									<div class="qty-container">
										{{-- <p>Jumlah</p> --}}
										<button class="qty-btn-minus btn-light" type="button"><i class="icon-minus"></i></button>
										<input type="text" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" name="quantity" value="{{$item->quantity}}" class="input-qty"/>
										<button class="qty-btn-plus btn-light" type="button"><i class="icon-plus"></i></button>
									</div>
								</td>
								<td class="options">
									<form action="{{ route('cart.update') }}" method="POST" id="cart-order-form">
										@csrf
										<input type="hidden" name="id" value="{{ $item->id }}" >
										<button type="submit" class="btn btn-success">Ubah</button>
									</form>
									<form action="{{ route('cart.remove') }}" method="POST">
										@csrf
										<input type="hidden" name="id" value="{{ $item->id }}" >
										<button type="submit" class="btn btn-danger">Hapus</button>
									</form>
								</td>
							</tr>
							</form>
							@endforeach
							@if(count($cartItems)==0)
							<tr>
								<td colspan="4">Belum ada item</td>
							</tr>
							@endif
						</tbody>
					</table>
				</div>
				</div>
				<!-- /col -->

				<aside class="col-lg-4" id="sidebar" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">

				<div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; transform: none; left: 942.5px; top: 0px;"><div class="box_detail">
						<div id="total_cart">
							Total <span class="float-end">Rp{{ number_format(Cart::getTotal(),0) }}</span>
						</div>
						@if(count($cartItems) > 0)
							@if (!Auth::check())
							@if(session('errorlogin'))
								<div class="alert alert-danger" data-duration="{{ session('alert-duration', 2000) }}">
									{{ session('errorlogin') }}
								</div>
							@endif
								<form action="{{ route('login') }}" method="GET">
									@csrf
									<section id="booking">
										<div class="form-group">
											<label for="booking_date">Pilih Tanggal:</label>
											<input type="date" id="booking_date" name="booking_date" class="form-control" required>
										</div>
										<!-- Tambahkan input untuk setiap item di cart -->
										@foreach($cartItems as $item)
											<input type="hidden" name="item_id[]" value="{{ $item->id }}">
										@endforeach
									</section>
									<button type="submit" {{$isContinue?'':'disabled'}} class="btn_1 full-width purchase">Lanjutkan</button>
								</form>
							@else
								<form action="{{ route('cart.order') }}" method="POST">
									@csrf
									<section id="booking">
										<div class="form-group">
											<label for="booking_date">Pilih tanggal:</label>
											<input type="date" id="booking_date" name="booking_date" class="form-control" required>
										</div>
										<!-- Tambahkan input untuk setiap item di cart -->
										@foreach($cartItems as $item)
											<input type="hidden" name="item_id[]" value="{{ $item->id }}">
										@endforeach
									</section>
									<button type="submit" {{$isContinue?'':'disabled'}} id="btnLanjutkan" class="btn_1 full-width purchase">Lanjutkan</button>
								</form>
							@endif
						@else
							<a href="#" class="btn_3 full-width disabled">-- 0 --</a>
						@endif


						{{-- <div class="text-center"><small>No money charged in this step</small></div> --}}
					</div><div class="resize-sensor" style="position: absolute; inset: 0px; overflow: hidden; z-index: -1; visibility: hidden;"><div class="resize-sensor-expand" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0px; top: 0px; transition: all; width: 450px; height: 513px;"></div></div><div class="resize-sensor-shrink" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0; top: 0; transition: 0s; width: 200%; height: 200%"></div></div></div></div></aside>
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
	<!-- /bg_color_1 -->
</main>
@endsection
@section('scripts')
<script>
    var count=0;
    var buttonPlus  = $(".qty-btn-plus");
    var buttonMinus = $(".qty-btn-minus");

    $(".input-qty").on("keyup",function(e){
        var amount = $(this).val();
        // var value = $( 'input[name=plan]:checked' ).val();
    // $('.purchase').text('Tambah ke keranjang | Rp'+(format(amount*value)));
    });
var incrementPlus = buttonPlus.click(function() {
  var $n = $(this)
  .parent(".qty-container")
  .find(".input-qty");
  $n.val(Number($n.val())+1 );
  count=Number($n.val());
//   var value = $( 'input[name=plan]:checked' ).val();
//     $('.purchase').text('Tambah ke keranjang | Rp'+(format(value*count)));
});
var incrementMinus = buttonMinus.click(function() {
  var $n = $(this)
  .parent(".qty-container")
  .find(".input-qty");
  var amount = Number($n.val());
  if (amount > 0) {
    $n.val(amount-1);
    count=amount;
    // var value = $( 'input[name=plan]:checked' ).val();
    // $('.purchase').text('Tambah ke keranjang | Rp'+(format(value*count)));
  }
});


var format = function(num){
      var str = num.toString().replace("", ""), parts = false, output = [], i = 1, formatted = null;
      if(str.indexOf(".") > 0) {
        parts = str.split(".");
        str = parts[0];
      }
      str = str.split("").reverse();
      for(var j = 0, len = str.length; j < len; j++) {
        if(str[j] != ",") {
          output.push(str[j]);
          if(i%3 == 0 && j < (len - 1)) {
            output.push(",");
          }
          i++;
        }
      }
      formatted = output.reverse().join("");
      return("" + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
    };
</script>
<script>
	var alertElements = document.querySelectorAll('.alert[data-duration]');
	alertElements.forEach(function(element) {
		var duration = parseInt(element.getAttribute('data-duration'));
		setTimeout(function() {
			element.style.display = 'none';
		}, duration);
	});
</script>
<script>
    // Skrip JavaScript untuk menampilkan pesan error dan memproses pemesanan dengan AJAX

    var errorAlert = document.getElementById('error-alert');
    var btnLanjutkan = document.getElementById('btnLanjutkan');
    var form = document.getElementById('cart-order-form');

    var bookingDate = document.getElementById('booking_date');
	bookingDate.addEventListener('change',function(){
		btnLanjutkan.disabled=false;
	});

    btnLanjutkan.addEventListener('click', function() {
		var bookingDate = document.getElementById('booking_date').value;
        if (!bookingDate) {
            alert('Silakan isi kolom Booking Date terlebih dahulu.');
            return;
        }
        btnLanjutkan.disabled = true;
        errorAlert.style.display = 'none';
		
        // Kirim permintaan AJAX untuk memproses pemesanan
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '{{ route('cart.order') }}', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        var bdate = new Date(bookingDate);
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    var response = JSON.parse(xhr.responseText);
                    if (response.success) {
                        // Jika pemesanan berhasil, tampilkan pesan sukses dan hapus form
                        form.style.display = 'none';
                        window.location.href = '{{ route('histori.pemesanan') }}';
                    } else {
                        // Jika stok habis,
                        var res ='<h4>Pemesanan Gagal!</h4><ul>';
						if(response.data){
							response.data.forEach(d => {
							var msg = document.getElementById('kuota_id'+d.id);
							msg.innerHTML="Kuota Habis!";
							msg.classList.add("sold-out");
							res+='<li>Kuota <b>'+d.name+'</b> di tgl '+bdate.getDate()+'-'+(bdate.getMonth()+1)+'-'+bdate.getFullYear()+' telah habis!</li>'
						});
						res+='<ul>Ganti tgl atau hapus item untuk melanjutkan.';
						}
                         errorAlert.innerHTML = res;
                         errorAlert.style.display = 'block';
						// setTimeout(function() {
                        //     errorAlert.style.display = 'none';
                        // }, 2000);
                    }
                } else {
                    // Jika ada kesalahan
                    errorAlert.textContent = 'Terjadi kesalahan saat memproses permintaan.';
                    errorAlert.style.display = 'block';
                }
                // Nyalakan kembali tombol lanjutkan setelah permintaan selesai diproses
                btnLanjutkan.disabled = false;
            }
        };
        // Kirim data item yang ingin dipesan (misalnya, item_id) ke server
        var formData = new FormData(form);
		formData.append('booking_date', bookingDate);
        xhr.send(new URLSearchParams(formData).toString());
    });
</script>
@stop
