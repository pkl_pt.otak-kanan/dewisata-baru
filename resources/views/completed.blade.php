@extends("layouts.front")
@section("content")
<main>
		<div class="hero_in cart_section last">
			<div class="wrapper">
				<div class="container">
					<!-- End bs-wizard -->
					<div id="confirm">
						<h3 style="color: white">Order completed!</h3>
					</div>
				</div>
			</div>
		</div>
		<!--/hero_in-->
	</main>
@endsection