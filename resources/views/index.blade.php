@extends("layouts.front")
@section("content")
<main>
    <section class="hero_single version_2">
        <div class="wrapper">
            <div class="container">
                <h3>Ojo Panik, Ayo Piknik!</h3>
                <p>Butuh piknik kemana? sama dewisata aja.</p>
                {{-- <form>
                    <div class="row g-0 custom-search-input-2">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Hotel, Kota...">
                                <i class="icon_pin_alt"></i>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <input class="form-control" type="text" name="dates" placeholder="Kapan..">
                                <i class="icon_calendar"></i>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="panel-dropdown">
                                <a href="#">Tamu <span class="qtyTotal">1</span></a>
                                <div class="panel-dropdown-content">
                                    <!-- Quantity Buttons -->
                                    <div class="qtyButtons">
                                        <label>Dewasa</label>
                                        <input type="text" name="qtyInput" value="1">
                                    </div>
                                    <div class="qtyButtons">
                                        <label>Anak</label>
                                        <input type="text" name="qtyInput" value="0">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <input type="submit" class="btn_search" value="Cari">
                        </div>
                    </div>
                    <!-- /row -->
                </form> --}}
            </div>
        </div>
    </section>
    <!-- /hero_single -->

    <div class="container container-custom margin_80_0">
        <div class="main_title_2">
            <span><em></em></span>
            <h2>Rekomendasi Kami</h2>
            <p>Jelajahi alam dan bersenang-senang</p>
        </div>
        <div id="reccomended" class="owl-carousel owl-theme">
        @foreach($products as $p)
            <div class="item">
                <div class="box_grid">
                    <figure>
                        <a href="#0" class="wish_bt"></a>
                        <a href="/detail/{{$p->slug}}"><img src="{{ url('/images/'.$p->image) }}" class="img-fluid" alt="" width="800" height="533"><div class="read_more"><span>Read more</span></div></a>
                        @if ( $p->kuota > 0)
                        <small>Tersisa {{$p->kuota}} Kuota</small>
                        @endif
                    </figure>
                    <div class="wrapper">
                        <h3><a href="/detail/{{$p->slug}}">{{$p->name}}</a></h3>

                    </div>
                    <ul>
                        {{-- <li><i class="icon-money-2"></i>Penawaran </li> --}}
                        <li><div class="score">
                            {{-- <span>Superb<em>350 Reviews</em></span><strong>8.9</strong> --}}
                            <a class="btn_1 rounded" href="/detail/{{$p->slug}}">Pesan Sekarang</a>
                        </div></li>
                    </ul>
                </div>
            </div>
            <!-- /item -->
        @endforeach

        </div>
        <!-- /carousel -->
        <p class="btn_home_align"><a href="/list" class="btn_1 rounded">Lihat semua</a></p>
        <hr class="large">
    </div>
    <!-- /container -->

    <div class="container container-custom margin_30_95">
        <section class="add_bottom_45">
            <div class="main_title_3">
                <span><em></em></span>
                <h2>FUN ACTIVITY TRIP</h2>
                <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
            </div>
            <div class="row">
            @foreach($products as $p)
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <a href="/detail/{{$p->slug}}" class="grid_item">
                        <figure>
                            {{-- <div class="score"><strong>8.9</strong></div> --}}
                            <img src="{{ url('/images/'.$p->image) }}" class="img-fluid" alt="">
                            <div class="info">
                                {{-- <div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div> --}}
                                <h3>{{$p->name}}</h3>
                            </div>
                        </figure>
                    </a>
                </div>
                <!-- /grid_item -->
            @endforeach
            </div>
            <!-- /row -->
            @php
            $productsCount = app('App\Http\Controllers\ProductListController')->getProductsCount();
          @endphp
            <a href="/list"><strong>Lihat semua ({{$productsCount}}) <i class="arrow_carrot-right"></i></strong></a>
        </section>
        <!-- /section -->

        <section class="add_bottom_45">
            <div class="main_title_3">
                <span><em></em></span>
                <h2>FRESH OUTING PROGRAM</h2>
                <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
            </div>
            <div class="row">
                @foreach($products as $p)
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <a href="/detail/{{$p->slug}}" class="grid_item">
                        <figure>
                            {{-- <div class="score"><strong>8.9</strong></div> --}}
                            <img src="{{ url('/images/'.$p->image) }}" class="img-fluid" alt="">
                            <div class="info">
                                {{-- <div class="cat_star"><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i></div> --}}
                                <h3>{{$p->name}}</h3>
                            </div>
                        </figure>
                    </a>
                </div>
                <!-- /grid_item -->
            @endforeach
            </div>
            <!-- /row -->
            @php
            $productsCount = app('App\Http\Controllers\ProductListController')->getProductsCount();
          @endphp
            <a href="/list"><strong>Lihat semua ({{$productsCount}}) <i class="arrow_carrot-right"></i></strong></a>
        </section>
        <!-- /section -->

        <div class="banner mb-0">
            <div class="wrapper d-flex align-items-center opacity-mask" data-opacity-mask="rgba(0, 0, 0, 0.3)">
                <div>
                    <small>Adventure</small>
                    <h3>Your Perfect<br>Advenure Experience</h3>
                    <p>Activities and accommodations</p>
                    <a href="adventure.html" class="btn_1">Read more</a>
                </div>
            </div>
            <!-- /wrapper -->
        </div>
        <!-- /banner -->

    </div>
    <!-- /container -->

</main>
<!-- /main -->
@endsection
