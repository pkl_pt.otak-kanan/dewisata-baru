@extends("layouts.front")
@section("content")
<main>
    <section class="hero_in tours_detail">
        <div class="wrapper">
        </div>
    </section>
    <div class="bg_color_1">
        <div class="container margin_60_35">
            <div class="row">
                <div class="col-lg-8">
                    <section id="description">
                        <div class="image-fluid">
                        <p><img alt="" class="img-fluid" src="{{ url('/images/'.$product->image)}}"></p>
                        </div>
                        <div class="desc">
                        <h2>Description</h2>
                        {!!$product->description!!}
                        <!-- /row -->
                        </div>
                    </section>
                    <!-- /section -->

                </div>
                <!-- /col -->

                <aside class="col-lg-4" id="sidebar">
                  <form action="{{ route('cart.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{-- <div class="box_detail"> --}}
                        <div class="plans">
                          <input type="hidden" name="plan_id" id="plan_id" value=""/>
                            <div class="title">{{$product->name}}</div>
                            @foreach($product->prices as $key=>$price)
                            <label class="plan-item {{$price->name}}-plan" for="{{$price->name}}">
                              <input type="radio" id="{{$price->name}}" value="{{$price->price}}" data-id="{{$price->id}}" class="price" name="plan" />
                              <div class="plan-content">
                                <div class="plan-details">
                                  <span>{{$price->name}}</span>
                                  <small>+ Rp{{number_format($price->price,0)}}</small>
                                </div>
                              </div>
                            </label>
                            @endforeach
                        <div class="qty-container">
                            {{-- <p>Jumlah</p> --}}
                            <button class="qty-btn-minus btn-light" type="button"><i class="icon-minus"></i></button>
                            <input type="text" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" name="qty" value="1" class="input-qty"/>
                            <button class="qty-btn-plus btn-light" type="button"><i class="icon-plus"></i></button>
                        </div>
                        <button type="submit" class="btn_1 full-width purchase" disabled="disabled" id="cart">Tambah Ke keranjang | Rp0</button>
                      </form>
                        {{-- <div class="text-center"><small>No money charged in this step</small></div> --}}
                    {{-- </div> --}}
                    {{-- <ul class="share-buttons">
                        <li><a class="fb-share" href="#0"><i class="social_facebook"></i> Share</a></li>
                        <li><a class="twitter-share" href="#0"><i class="social_twitter"></i> Tweet</a></li>
                        <li><a class="gplus-share" href="#0"><i class="social_googleplus"></i> Share</a></li>
                    </ul> --}}
                </div>
                </aside>
            </div>
            <!-- /row -->
            <div class="row">
              <div class="isotope-wrapper">
                <h4>Produk Lainnya</h4>
                <div class="row">
                    @foreach($products as $product)
                    <div class="col-md-3 isotope-item popular" style="/* position: absolute; */ /* left: 0px; */ /* top: 0px; */">
                        <div class="box_grid">
                            <figure>
                                <a href="#0" class="wish_bt"></a>
                                <a href="/detail/{{$product->slug}}"><img src="{{ url('/images/'.$product->image) }}" class="img-fluid" alt="" width="800" height="533"><div class="read_more"><span>Read more</span></div></a>
                                @if ( $product->kuota > 0)
                                <small>Tersisa {{$product->kuota}} Kuota</small>
                                @endif
                            </figure>
                            <div class="wrapper">
                                <h3><a href="/detail/{{$product->slug}}">{{$product->name}}</a></h3>
                            </div>
                            <ul>
                                {{-- <li><i class="icon_clock_alt"></i> 1h 30min</li> --}}
                                <li>
                                    <div class="score">
                                        <a class="btn_1 rounded" href="/detail/{{$product->slug}}">Pesan Sekarang</a>
                                </div></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /box_grid -->
                    @endforeach
                </div>
                <!-- /row -->
            </div>
            </div>
        </div>
        <!-- /container -->
    </div>
    <!-- /bg_color_1 -->
</main>
<!--/main-->
@endsection
@section('scripts')
<script>
    var count=1;
    var buttonPlus  = $(".qty-btn-plus");
    var buttonMinus = $(".qty-btn-minus");

    $(".input-qty").on("keyup",function(e){
        var amount = $(this).val();
        if(amount<=0){
          count=0;
          $(this).value=0;
          $('#cart').prop('disabled', true)
        }else{
          count=amount;
          $('#cart').prop('disabled', false)
        }
        var value = $( 'input[name=plan]:checked' ).val();
        if(value>0){
    // $('.purchase').addAttr('href','/cart');
    $('.purchase').text('Tambah ke keranjang | Rp'+(format(amount*value)));
  }else{
    $('.purchase').removeAttr('href');
  }
    });
var incrementPlus = buttonPlus.click(function() {
  var $n = $(this)
  .parent(".qty-container")
  .find(".input-qty");
  $n.val(Number($n.val())+1 );
  count=Number($n.val());
  var value = $( 'input[name=plan]:checked' ).val();
  if(!value){
          value=0;
        }
  if(value>0){
    // $('.purchase').attr('href','/cart');
    // $('.purchase').removeClass('disabled');
    $('#cart').prop('disabled', false)
    $('#cart').text('Tambah ke keranjang | Rp'+(format(value*count)));
  }else{
    // $('.purchase').removeAttr('href');
    // $('.purchase').addClass('disabled');
    $('#cart').prop('disabled', true)
  }
});
var incrementMinus = buttonMinus.click(function() {
  var $n = $(this)
  .parent(".qty-container")
  .find(".input-qty");
  var amount = Number($n.val());
  if (amount > 1) {
    $n.val(amount-1);
    count=amount-1;
    var value = $( 'input[name=plan]:checked' ).val();
    if(!value){
          value=1;
        }
  if((value*count)>1){
    // $('.purchase').attr('href','/cart');
    // $('.purchase').removeClass('disabled');
    $('#cart').prop('disabled', false)
    $('#cart').text('Tambah ke keranjang | Rp'+(format(value*count)));
  }else{
    // $('.purchase').removeAttr('href');
    // $('.purchase').addClass('disabled');
    $('#cart').prop('disabled', true)
  }
  }
});

$('input[name=plan]').change(function(){
var value = $( 'input[name=plan]:checked' ).val();
var dataId = $( 'input[name=plan]:checked').attr("data-id");
$("#plan_id").val(dataId);
console.log(count);
if((value*count)>0){
    // $('.purchase').attr('href', '/cart');
    // $('.purchase').removeClass('disabled');
    $('#cart').prop('disabled', false)
    $('#cart').text('Tambah ke keranjang | Rp'+(format(value*count)));
  }else{
    // $('.purchase').addClass('disabled');
    // $('.purchase').removeAttr('href');
    $('#cart').prop('disabled', true)
  }
});

var format = function(num){
      var str = num.toString().replace("", ""), parts = false, output = [], i = 1, formatted = null;
      if(str.indexOf(".") > 0) {
        parts = str.split(".");
        str = parts[0];
      }
      str = str.split("").reverse();
      for(var j = 0, len = str.length; j < len; j++) {
        if(str[j] != ",") {
          output.push(str[j]);
          if(i%3 == 0 && j < (len - 1)) {
            output.push(",");
          }
          i++;
        }
      }
      formatted = output.reverse().join("");
      return("" + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
    };
</script>
@stop
