@extends("layouts.front")
@section("content")
<main>
    <section class="hero_in tours_detail">
        <div class="wrapper">
            {{-- <div class="container">
                <h1 class="fadeInUp"><span></span>BALI MILLENIAL EAST NUSA PENIDA</h1>
            </div>
            <span class="magnific-gallery">
                <a href="img/gallery/tour_list_1.jpg" class="btn_photos" title="Photo title" data-effect="mfp-zoom-in">View photos</a>
                <a href="img/gallery/tour_list_2.jpg" title="Photo title" data-effect="mfp-zoom-in"></a>
                <a href="img/gallery/tour_list_3.jpg" title="Photo title" data-effect="mfp-zoom-in"></a>
            </span> --}}
        </div>
    </section>
    <!--/hero_in-->

    <div class="container margin_60_35" style="transform: none;">
        <div class="row" style="transform: none;">
            <aside class="col-lg-3" id="sidebar" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">

                <!--/filters col-->
            <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; transform: none; top: 0px; left: 62.5px;"><div id="filters_col">
                    <a data-bs-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt">Filters </a>
                    <div class="collapse show" id="collapseFilters">
                        <div class="filter_type">
                            <h6>Kategori</h6>
                            <ul>
                                @foreach($categories as $c)
                                <li>
                                    <label class="container_check">{{$c->name}} <small>({{$c->subcategory->count()}})</small>
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        {{-- <div class="filter_type">
                            <h6>Harga</h6>
                            <span class="irs js-irs-0"><span class="irs"><span class="irs-line" tabindex="-1"><span class="irs-line-left"></span><span class="irs-line-mid"></span><span class="irs-line-right"></span></span><span class="irs-min" style="display: none; visibility: visible;">0</span><span class="irs-max" style="display: none; visibility: visible;">1</span><span class="irs-from" style="visibility: visible; left: 13.319129%;">Min. 60</span><span class="irs-to" style="visibility: visible; left: 56.636679%;">Min. 130</span><span class="irs-single" style="visibility: hidden; left: 25.688723%;">Min. 60 — Min. 130</span></span><span class="irs-grid"></span><span class="irs-bar" style="left: 21.818182%; width: 43.838384%;"></span><span class="irs-shadow shadow-from" style="display: none;"></span><span class="irs-shadow shadow-to" style="display: none;"></span><span class="irs-slider from" style="left: 18.787879%;"></span><span class="irs-slider to type_last" style="left: 62.626263%;"></span></span><input type="text" id="range" name="range" value="" class="irs-hidden-input" readonly="">
                        </div> --}}
                    </div>
                    <!--/collapse -->
                </div><div class="resize-sensor" style="position: absolute; inset: 0px; overflow: hidden; z-index: -1; visibility: hidden;"><div class="resize-sensor-expand" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0px; top: 0px; transition: all; width: 340px; height: 1557px;"></div></div><div class="resize-sensor-shrink" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0; top: 0; transition: 0s; width: 200%; height: 200%"></div></div></div></div></aside>
            <!-- /aside -->

            <div class="col-lg-9">
                <div class="isotope-wrapper">
                    <div class="row">
                        @foreach($products as $product)
                        <div class="col-md-4 isotope-item popular" style="/* position: absolute; */ /* left: 0px; */ /* top: 0px; */">
                            <div class="box_grid">
                                <figure>
                                    <a href="#0" class="wish_bt"></a>
                                    <a href="/detail/{{$product->slug}}"><img src="{{ url('/images/'.$product->image) }}" class="img-fluid" alt="" width="800" height="533"><div class="read_more"><span>Read more</span></div></a>
                                    @if ( $product->kuota > 0)
                                    <small>Tersisa {{$product->kuota}} Kuota</small>
                                    @endif
                                </figure>
                                <div class="wrapper">
                                    <h3><a href="/detail/{{$product->slug}}">{{$product->name}}</a></h3>
                                </div>
                                <ul>
                                    {{-- <li><i class="icon_clock_alt"></i> 1h 30min</li> --}}
                                    <li>
                                        <div class="score">
                                            <a class="btn_1 rounded" href="/detail/{{$product->slug}}">Pesan Sekarang</a>
                                    </div></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /box_grid -->
                        @endforeach
                    </div>
                    <!-- /row -->
                </div>
                <!-- /isotope-wrapper -->
                @if($products->count()==0)
                 <p class="text-center"><a href="#0" class="btn_1 rounded add_top_30">Belum ada produk yang tersedia!</a></p>
                @endif
            {{-- <p class="text-center"><a href="#0" class="btn_1 rounded add_top_30">Load more</a></p> --}}
            </div>
            <!-- /col -->
        </div>
    </div>
    <!-- /container -->


</main>
@endsection
