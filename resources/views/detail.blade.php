@extends("layouts.front")
@section("content")
<main>
    <section class="hero_in tours_detail">
        <div class="wrapper">
            {{-- <div class="container">
                <h1 class="fadeInUp"><span></span>BALI MILLENIAL EAST NUSA PENIDA</h1>
            </div>
            <span class="magnific-gallery">
                <a href="img/gallery/tour_list_1.jpg" class="btn_photos" title="Photo title" data-effect="mfp-zoom-in">View photos</a>
                <a href="img/gallery/tour_list_2.jpg" title="Photo title" data-effect="mfp-zoom-in"></a>
                <a href="img/gallery/tour_list_3.jpg" title="Photo title" data-effect="mfp-zoom-in"></a>
            </span> --}}
        </div>
    </section>
    <!--/hero_in-->

    <div class="bg_color_1">
        <div class="container margin_60_35">
            <div class="row">
                <div class="col-lg-8">
                    <section id="description">
                        <div class="image-fluid">
                        <p><img alt="" class="img-fluid" src="https://prod-mayang.sgp1.digitaloceanspaces.com/img/1184495/1667617072/eyJ3aWR0aCI6NjQwLCJoZWlnaHQiOjY0MCwia2Jfc2l6ZSI6NTMwLjkxNn0%3D.jpeg"></p>
                        </div>
                        <div class="desc">
                        <h2>Description</h2>
                        <p>Mea appareat omittantur eloquentiam ad, nam ei quas <strong>oportere democritum</strong>. Prima causae admodum id est, ei timeam inimicus sed. Sit an meis aliquam, cetero inermis vel ut. An sit illum euismod facilisis, tamquam vulputate pertinacia eum at.</p>
                        <div class="row">
                            <div class="col-lg-6">
                                <ul class="bullets">
                                    <li>Tiket masuk</li>
                                    <li>Tiket konser</li>
                                    <li>Festival Kuliner</li>
                                    <li>Arak The World</li>
                                </ul>
                            </div>
                        </div>
                        <!-- /row -->
                        </div>
                    </section>
                    <!-- /section -->

                </div>
                <!-- /col -->
                
                <aside class="col-lg-4" id="sidebar">
                    {{-- <div class="box_detail"> --}}
                        <div class="plans">
                            <div class="title">BALI COMBO EXOTIC NUSA PENIDA</div>
                            <label class="plan-item basic-plan" for="basic">
                              <input checked type="radio" value="200000" class="price" name="plan" id="basic" />
                              <div class="plan-content">
                                <div class="plan-details">
                                  <span>ONE DAY TICKET</span>
                                  <small>+ Rp200.000</small>
                                </div>
                              </div>
                            </label>
                            <label class="plan-item complete-plan" for="complete">
                              <input type="radio" id="complete" value="500000" class="price" name="plan" />
                              <div class="plan-content">
                                <div class="plan-details">
                                  <span>TWO DAYS TICKET</span>
                                  <small>+ Rp500.000</small>
                                </div>
                              </div>
                            </label>
                        <div class="qty-container">
                            {{-- <p>Jumlah</p> --}}
                            <button class="qty-btn-minus btn-light" type="button"><i class="icon-minus"></i></button>
                            <input type="text" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" name="qty" value="0" class="input-qty"/>
                            <button class="qty-btn-plus btn-light" type="button"><i class="icon-plus"></i></button>
                        </div>
                        <a href="/cart" class="btn_1 full-width purchase">Tambah Ke keranjang | Rp200000</a>
                        {{-- <div class="text-center"><small>No money charged in this step</small></div> --}}
                    {{-- </div> --}}
                    {{-- <ul class="share-buttons">
                        <li><a class="fb-share" href="#0"><i class="social_facebook"></i> Share</a></li>
                        <li><a class="twitter-share" href="#0"><i class="social_twitter"></i> Tweet</a></li>
                        <li><a class="gplus-share" href="#0"><i class="social_googleplus"></i> Share</a></li>
                    </ul> --}}
                </div>
                </aside>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /bg_color_1 -->
</main>
<!--/main-->
@endsection
@section('scripts')
<script>
    var count=0;
    var buttonPlus  = $(".qty-btn-plus");
    var buttonMinus = $(".qty-btn-minus");

    $(".input-qty").on("keyup",function(e){
        var amount = $(this).val();
        var value = $( 'input[name=plan]:checked' ).val();
    $('.purchase').text('Tambah ke keranjang | Rp'+(format(amount*value)));
    });
var incrementPlus = buttonPlus.click(function() {
  var $n = $(this)
  .parent(".qty-container")
  .find(".input-qty");
  $n.val(Number($n.val())+1 );
  count=Number($n.val());
  var value = $( 'input[name=plan]:checked' ).val();
    $('.purchase').text('Tambah ke keranjang | Rp'+(format(value*count)));
});
var incrementMinus = buttonMinus.click(function() {
  var $n = $(this)
  .parent(".qty-container")
  .find(".input-qty");
  var amount = Number($n.val());
  if (amount > 0) {
    $n.val(amount-1);
    count=amount;
    var value = $( 'input[name=plan]:checked' ).val();
    $('.purchase').text('Tambah ke keranjang | Rp'+(format(value*count)));
  }
});

$('input[name=plan]').change(function(){
var value = $( 'input[name=plan]:checked' ).val();
console.log(count);
$('.purchase').text('Tambah ke keranjang | Rp'+(format(value*count)));
});

var format = function(num){
      var str = num.toString().replace("", ""), parts = false, output = [], i = 1, formatted = null;
      if(str.indexOf(".") > 0) {
        parts = str.split(".");
        str = parts[0];
      }
      str = str.split("").reverse();
      for(var j = 0, len = str.length; j < len; j++) {
        if(str[j] != ",") {
          output.push(str[j]);
          if(i%3 == 0 && j < (len - 1)) {
            output.push(",");
          }
          i++;
        }
      }
      formatted = output.reverse().join("");
      return("" + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
    };
</script>
@stop