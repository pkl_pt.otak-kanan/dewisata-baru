@extends('layouts.front')

@section('content')
<main>
    <section class="hero_in tours_detail">
        <div class="wrapper">
        </div>
    </section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title text-center">My Profile</h3>
                        <div class="d-flex justify-content-center mb-3">
                            <i class="bi bi-person-circle fs-1" style="font-size: 2.5rem;"></i>
                        </div>
                        <form action="{{ route('update-profile') }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" name="name" class="form-control" value="{{ $user->name }}" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" name="email" class="form-control" value="{{ $user->email }}" required>
                            </div>
                            <div class="form-group">
                                <label for="whatsapp">No. Whatsapp:</label>
                                <input type="whatsapp" name="whatsapp" class="form-control" value="{{ $user->whatsapp }}" required>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">Update Profile</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection